#!/bin/env bash

if [ $# -ne 1 ]
then
  echo "usage: <jarfile>"
  exit 1
fi

INPUTS=(
  [1]='f:benchmark/Dijkstra-ring.json nomutex'
  [2]='Tf:benchmark/left-dining-philosophers.json deadlock'
  [3]='Tf:benchmark/mux-array.json nomutex'
  [4]='Tf:benchmark/res-allocator.json nomutex'
  [5]='Tf:benchmark/Berkeley.json exclusiveexclusive'
  [6]='Tf:benchmark/dragon.json dirtyshareddirty'
  [7]='Tf:benchmark/firefly.json exclusiveexclusive'
  [8]='Tf:benchmark/firefly.json dirtyshared'
  [9]='Tf:benchmark/firefly.json dirtyexclusive'
  [10]='Tf:benchmark/firefly.json dirtydirty'
  [11]='Tf:benchmark/Illinois.json dirtyshared'
  [12]='Tf:benchmark/Illinois.json dirtydirty'
)

for INDEX in ${!INPUTS[@]}
do
  echo "${INDEX} of 61"
  MODES=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$1}" - )
  FILE=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$2}" - )
  for MODE in ${MODES}
    do
      echo timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
      timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
    done
done

