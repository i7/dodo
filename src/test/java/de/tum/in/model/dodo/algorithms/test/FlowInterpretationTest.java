package de.tum.in.model.dodo.algorithms.test;

import com.google.common.collect.Streams;
import de.tum.in.model.dodo.algorithms.FlowInterpretation;
import de.tum.in.model.dodo.algorithms.Learning;
import de.tum.in.model.dodo.algorithms.Learning.TeacherLearner;
import de.tum.in.model.dodo.algorithms.Learning.Unlearnable;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.graphs.Graph;
import net.automatalib.serialization.dot.DOTSerializationProvider;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.builders.DFABuilder;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class FlowInterpretationTest {
    private RegularTransitionSystem rts;
    private FlowInterpretation flow;

    @Before
    public void setUp() throws IOException {
        this.rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        this.flow = new FlowInterpretation(this.rts);
    }

    private CompactNFA<String> oneWordAutomaton(Word<String> word, Alphabet<String> alphabet) {
        CompactNFA<String> result = new CompactNFA<>(alphabet);
        Integer previous = result.addInitialState();
        for(String letter: word) {
            Integer next = result.addState();
            result.addTransition(previous, letter, next);
            previous = next;
        }
        result.setAccepting(previous, true);
        return result;
    }

    @Test
    public void testOneShot() {
        List<Word<String>> goodWords = List.of(
                Word.fromSymbols("t"),
                Word.fromSymbols("t", "n"),
                Word.fromSymbols("n", "t"),
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t")
        );
        List<Word<String>> badWords = List.of(
                Word.fromSymbols("n"),
                Word.fromSymbols("n", "n"),
                Word.fromSymbols("t", "t"),
                Word.fromSymbols("n", "n", "n"),
                Word.fromSymbols("n", "t", "t"),
                Word.fromSymbols("t", "n", "t"),
                Word.fromSymbols("t", "t", "n"),
                Word.fromSymbols("t", "t", "t")
        );
        for(Word<String> word: goodWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            Word<LetterPair<String, String>> oneShotResult = this.flow.oneShot(wordAutomaton).findAcceptedWord().orElse(null);
            assertNotNull("one shot falsely disproves " + word, oneShotResult);
            assertEquals(word.length(), oneShotResult.length());
            for(int i=0; i<word.length(); i++) {
                assertEquals(oneShotResult.getSymbol(i).from(), i==0 ? "t" : "n");
                assertEquals(oneShotResult.getSymbol(i).to(), word.getSymbol(i));
            }
        }
        for(Word<String> word: badWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            assertNull("one shot cannot disprove " + word, this.flow.oneShot(wordAutomaton).findAcceptedWord().orElse(null));
        }
    }

    @Test
    public void outputLearnedInductiveStatements() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("benchmark", "token-passing-no-invariant.json")));
        FlowInterpretation flow = new FlowInterpretation(rts);
        CompactNFA<Set<String>> nonInductiveStatements = flow.getNonInductive(rts);
        CompactDFA<Set<String>> nonInitialStatements = AutomatonBuilders.newDFA(flow.getInterpretationAlphabet(rts))
            .withInitial(0)
            .from(0)
            .on(Set.of("t"), Set.of("t", "n"))
            .to(1)
            .from(0)
            .on(Set.of("n"), Set.of())
            .to(2)
            .from(2)
            .on(Set.of("t"), Set.of())
            .to(2)
            .from(2)
            .on(Set.of("n"), Set.of("t", "n"))
            .to(1)
            .from(1)
            .on(Set.of("n"), Set.of("t", "n"))
            .to(3)
            .from(1)
            .on(Set.of("t"), Set.of())
            .to(1)
            .from(3)
            .on(Set.of("t"), Set.of(), Set.of("n"), Set.of("t", "n"))
            .to(3)
            .withAccepting(0, 2, 3)
            .create();
            CompactDFA<Set<String>> inductiveStatements = DFAs.complement(
                DFAs.minimize(
                    DFAs.or(
                        nonInitialStatements,
                        NFAs.determinize(nonInductiveStatements, nonInductiveStatements.getInputAlphabet()), nonInductiveStatements.getInputAlphabet()), nonInductiveStatements.getInputAlphabet()), nonInductiveStatements.getInputAlphabet());
     
        System.out.println(inductiveStatements.size());
        DOTSerializationProvider.getInstance().writeModel(System.out, (Graph<Object, Object>) inductiveStatements.graphView());
    }

    @Test
    public void outputInductiveStatements() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("benchmark", "token-passing-no-invariant.json")));
        FlowInterpretation flow = new FlowInterpretation(rts);
        try {
            List<TeacherLearner<?>> l = Learning.learn(rts, List.of(flow), rts.getProperty("nonreach"));
            TeacherLearner<?> t = l.get(0);
            DFA c = t.getLearner().getHypothesisModel();
            DOTSerializationProvider.getInstance().writeModel(System.out, (Graph<Object, Object>) c.transitionGraphView(t.getInputs()));
        } catch (Unlearnable e) {
            fail("this property should be learnable");
        }
    }
    
    @Test
    public void testDisprove() {
        CompactNFA<Set<String>> nonInductive = this.flow.getNonInductive(this.rts);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.flow.getInterpretationAutomaton(this.rts);
        for(Word<String> origin: Set.of(
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("t", "t", "n"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "t", "t"))) {
                assertNull(this.flow.disprove(this.rts, origin, origin));
                assertNull(this.flow.disprove(this.rts, target, target));
                Word<Set<String>> separator = this.flow.disprove(this.rts, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue("flow interpretation rejects " + origin + " and " + separator, interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse("flow interpretation accepts " + target + " and " + separator, interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }
}