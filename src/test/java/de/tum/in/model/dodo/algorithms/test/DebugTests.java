package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.FlowInterpretation;
import de.tum.in.model.dodo.algorithms.TransducerStep;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.NFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.graphs.Graph;
import net.automatalib.serialization.dot.DOTSerializationProvider;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class DebugTests {
    @Test
    public void testDijkstraMutEx() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("benchmark", "DijkstraMutEx.json")));
        TrapInterpretation trap = new TrapInterpretation(rts);
        assertFalse(trap.isNonInductive(rts,
                Word.fromSymbols(
                        Set.of(
                                "1fp",
                                "1fP",
                                "1Fp",
                                "2Fp",
                                "3Fp",
                                "4Fp",
                                "5Fp",
                                "6Fp",
                                "1FP",
                                "2FP",
                                "3FP",
                                "4FP",
                                "5FP",
                                "6FP"
                        ),
                        Set.of()
                        )));
    }

    @Test
    public void outputInductiveStatements() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("benchmark", "token-passing-no-invariant.json")));
        FlowInterpretation flow = new FlowInterpretation(rts);
        TrapInterpretation trap = new TrapInterpretation(rts);
        DFA allInductiveStatementsFlow = DFAs.minimize(
            DFAs.complement(
                NFAs.determinize(
                    flow.getNonInductive(rts),
                    flow.getInterpretationAlphabet(rts)),
                flow.getInterpretationAlphabet(rts)
            )
        );
        DFA allInductiveStatementsTrap = DFAs.minimize(
            DFAs.complement(
                NFAs.determinize(
                    trap.getNonInductive(rts),
                    trap.getInterpretationAlphabet(rts)),
                trap.getInterpretationAlphabet(rts)
            )
        );
        OutputStream inductiveFlowFile = Files.newOutputStream(Path.of("dot-files", "inductive-flows.dot"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        OutputStream inductiveTrapFile = Files.newOutputStream(Path.of("dot-files", "inductive-traps.dot"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        DOTSerializationProvider.getInstance().writeModel(inductiveFlowFile, (Graph<Object, Object>) allInductiveStatementsFlow.transitionGraphView(flow.getInterpretationAlphabet(rts)));
        DOTSerializationProvider.getInstance().writeModel(inductiveTrapFile, (Graph<Object, Object>) allInductiveStatementsTrap.transitionGraphView(trap.getInterpretationAlphabet(rts)));
        DFA flowAbstraction = DFAs.minimize(
            NFAs.determinize(
                TransducerStep.stepAutomaton(
            rts.getInitialer(),
            DFAs.complement(
                NFAs.determinize(    
                    flow.nonAbstractlyReachable(rts, allInductiveStatementsFlow, flow.getInterpretationAlphabet(rts)),
                    rts.getTransducer().getInputAlphabet()
                ), rts.getTransducer().getInputAlphabet()
            )
        )));
        DFA trapAbstraction = DFAs.minimize(
            NFAs.determinize(
        TransducerStep.stepAutomaton(
            rts.getInitialer(),
            DFAs.complement(
                NFAs.determinize(    
                    trap.nonAbstractlyReachable(rts, allInductiveStatementsTrap, trap.getInterpretationAlphabet(rts)),
                    rts.getTransducer().getInputAlphabet()
                ), rts.getTransducer().getInputAlphabet()
            )
        )));
        OutputStream flowAbstractionFile = Files.newOutputStream(Path.of("dot-files", "flow-abstraction.dot"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        OutputStream trapAbstractionFile = Files.newOutputStream(Path.of("dot-files", "trap-abstraction.dot"), StandardOpenOption.CREATE, StandardOpenOption.WRITE);
        DOTSerializationProvider.getInstance().writeModel(flowAbstractionFile, (Graph<Object, Object>) flowAbstraction.transitionGraphView(rts.getSigma()));
        DOTSerializationProvider.getInstance().writeModel(trapAbstractionFile, (Graph<Object, Object>) trapAbstraction.transitionGraphView(rts.getSigma()));
    }

    @Test
    public void testLeftHandPhilosopher() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("benchmark", "left-dining-philosophers.json")));
        CompactNFA<String> deadlock = rts.getProperty("deadlock");
        CompactNFA<LetterPair<String, String>> transducer = rts.getTransducer();
        Set<Integer> reachableStates = transducer.getInitialStates();
        Set<Integer> next = new HashSet<>();
        for(Integer c: reachableStates) {
            for(LetterPair<String, String> l: transducer.getLocalInputs(c)) {
                if(!l.from().equals("tf")) {
                    continue;
                }
                next.addAll(transducer.getSuccessors(c, l));
            }
        }
        reachableStates = next;
        next = new HashSet<>();
        for(Integer c: reachableStates) {
            for(LetterPair<String, String> l: transducer.getLocalInputs(c)) {
                if(!l.from().equals("eb")) {
                    continue;
                }
                next.addAll(transducer.getSuccessors(c, l));
            }
        }
        reachableStates = next;
        assertTrue(reachableStates.stream().noneMatch(transducer::isAccepting));
        assertTrue(deadlock.accepts(Word.fromSymbols("tf", "eb")));

        FlowInterpretation flow = new FlowInterpretation(rts);
        assertFalse(flow.isNonInductive(rts, Word.fromSymbols(Set.of(), Set.of("ef", "eb"), Set.of("tf", "hb", "eb"))));
    }

}
