package de.tum.in.model.dodo.specification.test;

import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import junit.framework.TestCase;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class RegularTransitionSystemTest extends TestCase {
    private RegularTransitionSystem rts;

    public void setUp() throws Exception {
        super.setUp();
        this.rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
    }

    public void testParsing() {
        assertNotNull("token passing could not be parsed", this.rts);
        assertEquals("expected number of states of initial automaton", 2, this.rts.getInitialer().size());
        assertEquals("expected number of states of transducer automaton", 3, this.rts.getTransducer().size());
    }

    public void testInitial() {
        assertTrue("accepts initial word t", this.rts.getInitialer().accepts(List.of("t")));
        assertFalse("reject initial word n", this.rts.getInitialer().accepts(List.of("n")));
        assertTrue("accepts initial word tn", this.rts.getInitialer().accepts(List.of("t", "n")));
        assertFalse("rejects initial word nn", this.rts.getInitialer().accepts(List.of("n", "n")));
        assertFalse("rejects initial word nt", this.rts.getInitialer().accepts(List.of("n", "t")));
        assertFalse("rejects initial word tt", this.rts.getInitialer().accepts(List.of("t", "t")));
    }
}