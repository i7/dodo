package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.*;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.ListAlphabet;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.junit.Assert.*;

public class OneShotTest {
    @Test
    public void testOneShot() throws IOException {
        Alphabet<String> alphabet = new ListAlphabet<>(List.of("tp", "np", "tq", "nq"));
        CompactNFA<String> badLanguage = AutomatonBuilders.newNFA(alphabet)
                // no, or many tokens
                .withInitial(0)
                .from(0)
                .on("np", "nq")
                .to(0)
                .from(0)
                .on("tp", "tq")
                .to(1)
                .from(1)
                .on("np", "nq")
                .to(1)
                .from(1)
                .on("tp", "tq")
                .to(2)
                .from(2)
                .on("tp", "np", "tq", "nq")
                .to(2)
                .withAccepting(0, 2)
                // no p
                .withInitial(3)
                .from(3)
                .on("nq", "tq")
                .to(3)
                .from(3)
                .on("np", "tp")
                .to(4)
                .from(4)
                .on("tp", "np", "tq", "nq")
                .to(4)
                .withAccepting(3)
                .create();

        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing-leader.json")));
        FlowInterpretation flow = new FlowInterpretation(rts);
        TrapInterpretation trap = new TrapInterpretation(rts);
        OneShot flowOneShot = flow.oneShot(badLanguage);
        OneShot trapOneShot = trap.oneShot(badLanguage);
        assertFalse(trapOneShot.findAcceptedWord().isEmpty());
        try {
            Learning.learn(rts, List.of(flow), badLanguage);
            fail();
        } catch (Learning.Unlearnable e) {
            CompactNFA<LetterPair<String, String>> nonReach = flow.nonAbstractlyReachable(rts, DFAs.complement(NFAs.determinize(flow.getNonInductive(rts)), flow.getInterpretationAlphabet(rts)), flow.getInterpretationAlphabet(rts));
            assertFalse(nonReach.accepts(e.unremovable));
        }
        assertFalse(flowOneShot.findAcceptedWord().isEmpty());
        flowOneShot = flow.oneShot(badLanguage);
        trapOneShot = trap.oneShot(badLanguage);
        assertTrue((new OneShot.CompositionalOneShot(List.of(flowOneShot, trapOneShot))).findAcceptedWord().isEmpty());
    }
}