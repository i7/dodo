package de.tum.in.model.dodo.algorithms.test;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import de.tum.in.model.dodo.algorithms.CrowdAbstractor;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.algorithms.Util;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.junit.Assert.*;

public class CrowdAbstractorTest {

    @Test
    public void getLanguage() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        TrapInterpretation interpretation = new TrapInterpretation(rts);
        CrowdAbstractor abstractor = new CrowdAbstractor(rts, interpretation, 2);
        Word<Set<String>> word = Word.fromSymbols(Set.of("t"), Set.of("t"), Set.of("n"), Set.of(), Set.of(), Set.of());
        assertTrue(abstractor.generalize(word));
        CompactDFA<Set<String>> expected = new CompactDFA<>(interpretation.getInterpretationAlphabet(rts));
        Map<Multiset<Set<String>>, Integer> states = new HashMap<>();
        Queue<Multiset<Set<String>>> workList = new LinkedList<>();
        Multiset<Set<String>> initial = HashMultiset.create();
        states.put(initial, expected.addInitialState());
        Multiset<Set<String>> exit = HashMultiset.create(List.of(Set.of("t"), Set.of("t"), Set.of("n"), Set.of(), Set.of()));
        states.put(exit, expected.addState(true));
        workList.add(initial);
        workList.add(exit);
        while(!workList.isEmpty()) {
            Multiset<Set<String>> current = workList.poll();
            for(Set<String> l: interpretation.getInterpretationAlphabet(rts)) {
                Multiset<Set<String>> next = HashMultiset.create(current);
                if(next.count(l) < 2) next.add(l);
                if(!states.containsKey(next)) {
                    states.put(next, expected.addState());
                    workList.add(next);
                }
                expected.addTransition(states.get(current), l, states.get(next));
            }
        }
        CompactDFA<Set<String>> minimal = DFAs.minimize(expected);
        assertEquals(19, minimal.size()); // all multi sets and sink
        assertTrue(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of("n"), Set.of(), Set.of())));
        assertTrue(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of("n"), Set.of(), Set.of(), Set.of())));
        assertTrue(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of("n"), Set.of(), Set.of(), Set.of("t"))));
        assertFalse(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of(), Set.of())));
        assertFalse(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of(), Set.of(), Set.of())));
        assertFalse(minimal.accepts(List.of(Set.of("t"), Set.of("t"), Set.of(), Set.of(), Set.of("t"))));

        CompactDFA<LetterPair<String, String>> abstraction = DFAs.complement(
                NFAs.determinize(
                        interpretation.nonAbstractlyReachable(rts, minimal, interpretation.getInterpretationAlphabet(rts)),
                        rts.getTransducer().getInputAlphabet()),
                rts.getTransducer().getInputAlphabet());

        assertNull(Util.getAcceptedWord(DFAs.xor(abstraction, abstractor.getAbstraction(), rts.getTransducer().getInputAlphabet())));
    }
}