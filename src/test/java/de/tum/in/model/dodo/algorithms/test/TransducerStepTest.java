package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.TransducerStep;
import de.tum.in.model.dodo.algorithms.Util;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TransducerStepTest {
    @Test
    public void testTransducerStep() {
        try {
            RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
            CompactDFA<String> oneStep = NFAs.determinize(TransducerStep.stepAutomaton(rts.getInitialer(), rts.getTransducer()));
            // should accept: n t n*
            CompactDFA<String> expectedResult = NFAs.determinize(AutomatonBuilders.newNFA(rts.getInitialer().getInputAlphabet())
                    .from(0)
                    .on("n")
                    .to(1)
                    .from(1)
                    .on("t")
                    .to(2)
                    .from(2)
                    .on("n")
                    .loop()
                    .withAccepting(2)
                    .withInitial(0)
                    .create()
            );
            assertTrue(expectedResult.accepts(Word.fromSymbols("n", "t")));
            CompactDFA<String> symmetricDifference = DFAs.minimize(DFAs.xor(oneStep, expectedResult, oneStep.getInputAlphabet()));
            System.out.println(Util.getAcceptedWord(oneStep));
            assertTrue(oneStep.accepts(Word.fromSymbols("n", "t")));
            assertTrue(DFAs.acceptsEmptyLanguage(symmetricDifference));
        }
        catch(IOException e) {
            fail(e.getMessage());
        }
    }

}