package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.BowAbstractor;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.algorithms.Util;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import static org.junit.Assert.*;

public class BowAbstractorTest {

    @Test
    public void getLanguage() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        TrapInterpretation interpretation = new TrapInterpretation(rts);
        BowAbstractor abstractor = new BowAbstractor(rts, interpretation);
        Word<Set<String>> word = Word.fromSymbols(Set.of("t"), Set.of("n"), Set.of("n"), Set.of("t"));
        assertTrue(abstractor.generalize(word));
        CompactDFA<Set<String>> expectedAbstraction = AutomatonBuilders.newDFA(interpretation.getInterpretationAlphabet(rts))
                .withInitial(1)
                .from(1)
                .on(Set.of("t"))
                .to(2)
                .from(2)
                .on(Set.of("n"))
                .to(3)
                .from(3)
                .on(Set.of("n"))
                .to(3)
                .from(3)
                .on(Set.of("t"))
                .to(4)
                .withAccepting(4)
                .create();
        CompactDFA<LetterPair<String, String>> inducedAbstraction = DFAs.complement(NFAs.determinize(interpretation.nonAbstractlyReachable(rts, expectedAbstraction, interpretation.getInterpretationAlphabet(rts))), abstractor.getAbstraction().getInputAlphabet());
        Word<LetterPair<String, String>> ce = Word.fromSymbols(LetterPair.of("t", "n"), LetterPair.of("t", "t"),LetterPair.of("t", "n"));
        assertNull(Util.getAcceptedWord(DFAs.xor(inducedAbstraction, abstractor.getAbstraction(), abstractor.getAbstraction().getInputAlphabet())));
    }
}