package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.RingAbstractor;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.algorithms.Util;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class RingAbstractorTest {
    @Test
    public void testRingAbstraction() throws IOException {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        TrapInterpretation interpretation = new TrapInterpretation(rts);
        RingAbstractor abstractor = new RingAbstractor(rts, interpretation);
        Word<Set<String>> word = Word.fromSymbols(Set.of("t"), Set.of("t"));
        assertTrue(abstractor.generalize(word));
        CompactDFA<LetterPair<String, String>> expectedAbstraction = AutomatonBuilders.newDFA(new ListAlphabet<>(List.of(LetterPair.of("n", "n"), LetterPair.of("n", "t"), LetterPair.of("t", "n"), LetterPair.of("t", "t"))))
                .withInitial(1)
                .from(1)
                .on(LetterPair.of("n", "n"))
                .to(1)
                .from(1)
                .on(LetterPair.of("n", "t"))
                .to(2)
                .from(1)
                .on(LetterPair.of("t", "n"))
                .to(3)
                .from(1)
                .on(LetterPair.of("t", "t"))
                .to(4)
                .from(2)
                .on(LetterPair.of("n", "n"))
                .to(2)
                .from(2)
                .on(LetterPair.of("n", "t"))
                .to(2)
                .from(2)
                .on(LetterPair.of("t", "n"))
                .to(4)
                .from(2)
                .on(LetterPair.of("t", "t"))
                .to(4)
                .from(3)
                .on(LetterPair.of("n", "n"))
                .to(3)
                .from(3)
                .on(LetterPair.of("n", "t"))
                .to(4)
                .from(3)
                .on(LetterPair.of("t", "n"))
                .to(3)
                .from(3)
                .on(LetterPair.of("t", "t"))
                .to(4)
                .from(4)
                .on(LetterPair.of("n", "n"))
                .to(4)
                .from(4)
                .on(LetterPair.of("n", "t"))
                .to(4)
                .from(4)
                .on(LetterPair.of("t", "n"))
                .to(4)
                .from(4)
                .on(LetterPair.of("t", "t"))
                .to(4)
                .withAccepting( 1, 2, 4)
                .create();
        CompactDFA<LetterPair<String, String>> atLeastTwo = AutomatonBuilders.newDFA(new ListAlphabet<>(List.of(LetterPair.of("n", "n"), LetterPair.of("n", "t"), LetterPair.of("t", "n"), LetterPair.of("t", "t"))))
                .withInitial(0)
                .from(0)
                .on(LetterPair.of("n", "n"), LetterPair.of("n", "t"), LetterPair.of("t", "n"), LetterPair.of("t", "t"))
                .to(1)
                .from(1)
                .on(LetterPair.of("n", "n"), LetterPair.of("n", "t"), LetterPair.of("t", "n"), LetterPair.of("t", "t"))
                .to(2)
                .from(2)
                .on(LetterPair.of("n", "n"), LetterPair.of("n", "t"), LetterPair.of("t", "n"), LetterPair.of("t", "t"))
                .to(2)
                .withAccepting(0, 1)
                .create();
        assertNull(Util.getAcceptedWord(DFAs.xor(DFAs.or(expectedAbstraction, atLeastTwo, atLeastTwo.getInputAlphabet()), abstractor.getAbstraction(), expectedAbstraction.getInputAlphabet())));
    }


}