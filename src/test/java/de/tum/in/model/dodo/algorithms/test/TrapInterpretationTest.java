package de.tum.in.model.dodo.algorithms.test;

import com.google.common.collect.Streams;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.algorithms.TrapInterpretationSAT;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.graphs.Graph;
import net.automatalib.serialization.dot.DOTSerializationProvider;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class TrapInterpretationTest {

    TrapInterpretation trapTokenPassing;
    TrapInterpretation trapTokenPassingNoCache;
    TrapInterpretation trapTrapTest;
    TrapInterpretationSAT trapTokenPassingSAT;
    TrapInterpretationSAT trapTrapTestSAT;
    RegularTransitionSystem rtsTokenPassing;
    RegularTransitionSystem rtsTrapTest;

    @Before
    public void setUp() throws IOException {
        this.rtsTokenPassing = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        this.trapTokenPassing = new TrapInterpretation(this.rtsTokenPassing);
        this.trapTokenPassingNoCache = new TrapInterpretation(this.rtsTokenPassing, false);
        this.trapTokenPassingSAT = new TrapInterpretationSAT(this.rtsTokenPassing);
        this.rtsTrapTest = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "trap-test.json")));
        this.trapTrapTest = new TrapInterpretation(this.rtsTrapTest);
        this.trapTrapTestSAT = new TrapInterpretationSAT(this.rtsTrapTest);
    }

    @Test
    public void testDisproveTrapTestSAT() {
        CompactNFA<Set<String>> nonInductive = this.trapTrapTestSAT.getNonInductive(this.rtsTrapTest);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.trapTrapTestSAT.getInterpretationAutomaton(this.rtsTrapTest);
        for(Word<String> origin: Set.of(Word.fromSymbols("t", "t", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("z", "z", "z"),
                    Word.fromSymbols("z", "z", "b"),
                    Word.fromSymbols("z", "b", "z"),
                    Word.fromSymbols("z", "b", "b"),
                    Word.fromSymbols("b", "z", "z"),
                    Word.fromSymbols("b", "z", "b"),
                    Word.fromSymbols("b", "b", "z"),
                    Word.fromSymbols("b", "b", "b"))) {
                assertNull(this.trapTrapTestSAT.disprove(this.rtsTrapTest, origin, origin));
                assertNull(this.trapTrapTestSAT.disprove(this.rtsTrapTest, target, target));
                Word<Set<String>> separator = this.trapTrapTestSAT.disprove(this.rtsTrapTest, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }


    @Test
    public void testDisproveTrapTest() {
        CompactNFA<Set<String>> nonInductive = this.trapTrapTest.getNonInductive(this.rtsTrapTest);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.trapTrapTest.getInterpretationAutomaton(this.rtsTrapTest);
        for(Word<String> origin: Set.of(Word.fromSymbols("t", "t", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("z", "z", "z"),
                    Word.fromSymbols("z", "z", "b"),
                    Word.fromSymbols("z", "b", "z"),
                    Word.fromSymbols("z", "b", "b"),
                    Word.fromSymbols("b", "z", "z"),
                    Word.fromSymbols("b", "z", "b"),
                    Word.fromSymbols("b", "b", "z"),
                    Word.fromSymbols("b", "b", "b"))) {
                assertNull(this.trapTrapTest.disprove(this.rtsTrapTest, origin, origin));
                assertNull(this.trapTrapTest.disprove(this.rtsTrapTest, target, target));
                Word<Set<String>> separator = this.trapTrapTest.disprove(this.rtsTrapTest, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }

    @Test
    public void testDisproveTokenPassingSAT() {
        CompactNFA<Set<String>> nonInductive = this.trapTokenPassingSAT.getNonInductive(this.rtsTokenPassing);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.trapTokenPassingSAT.getInterpretationAutomaton(this.rtsTokenPassing);
        for(Word<String> origin: Set.of(
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("t", "t", "n"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "t", "t"))) {
                assertNull(this.trapTokenPassingSAT.disprove(this.rtsTokenPassing, origin, origin));
                assertNull(this.trapTokenPassingSAT.disprove(this.rtsTokenPassing, target, target));
                Word<Set<String>> separator = this.trapTokenPassingSAT.disprove(this.rtsTokenPassing, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }

    private CompactNFA<String> oneWordAutomaton(Word<String> word, Alphabet<String> alphabet) {
        CompactNFA<String> result = new CompactNFA<>(alphabet);
        Integer previous = result.addInitialState();
        for(String letter: word) {
            Integer next = result.addState();
            result.addTransition(previous, letter, next);
            previous = next;
        }
        result.setAccepting(previous, true);
        return result;
    }

    @Test
    public void testOneShot() {
        RegularTransitionSystem rts = this.rtsTokenPassing;
        List<Word<String>> goodWords = List.of(
                Word.fromSymbols("t"),
                Word.fromSymbols("t", "n"),
                Word.fromSymbols("n", "t"),
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t")
        );
        List<Word<String>> badWords = List.of(
                Word.fromSymbols("n"),
                Word.fromSymbols("n", "n"),
                Word.fromSymbols("t", "t"),
                Word.fromSymbols("n", "n", "n"),
                Word.fromSymbols("n", "t", "t"),
                Word.fromSymbols("t", "n", "t"),
                Word.fromSymbols("t", "t", "n"),
                Word.fromSymbols("t", "t", "t")
        );
        for(Word<String> word: goodWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            Word<LetterPair<String, String>> oneShotResult = this.trapTokenPassing.oneShot(wordAutomaton).findAcceptedWord().orElse(null);
            assertNotNull("one shot falsely disproves " + word, oneShotResult);
            assertEquals(word.length(), oneShotResult.length());
            for(int i=0; i<word.length(); i++) {
                assertEquals(oneShotResult.getSymbol(i).from(), i==0 ? "t" : "n");
                assertEquals(oneShotResult.getSymbol(i).to(), word.getSymbol(i));
            }
        }
        for(Word<String> word: badWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            assertNull("one shot cannot disprove " + word, this.trapTokenPassing.oneShot(wordAutomaton).findAcceptedWord().orElse(null));
        }
    }

    @Test
    public void testOneShotNoCache() {
        RegularTransitionSystem rts = this.rtsTokenPassing;
        List<Word<String>> goodWords = List.of(
                Word.fromSymbols("t"),
                Word.fromSymbols("t", "n"),
                Word.fromSymbols("n", "t"),
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t")
        );
        List<Word<String>> badWords = List.of(
                Word.fromSymbols("n"),
                Word.fromSymbols("n", "n"),
                Word.fromSymbols("t", "t"),
                Word.fromSymbols("n", "n", "n"),
                Word.fromSymbols("n", "t", "t"),
                Word.fromSymbols("t", "n", "t"),
                Word.fromSymbols("t", "t", "n"),
                Word.fromSymbols("t", "t", "t")
        );
        for(Word<String> word: goodWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            Word<LetterPair<String, String>> oneShotResult = this.trapTokenPassingNoCache.oneShot(wordAutomaton).findAcceptedWord().orElse(null);
            assertNotNull("one shot falsely disproves " + word, oneShotResult);
            assertEquals(word.length(), oneShotResult.length());
            for(int i=0; i<word.length(); i++) {
                assertEquals(oneShotResult.getSymbol(i).from(), i==0 ? "t" : "n");
                assertEquals(oneShotResult.getSymbol(i).to(), word.getSymbol(i));
            }
        }
        for(Word<String> word: badWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            assertNull("one shot cannot disprove " + word, this.trapTokenPassingNoCache.oneShot(wordAutomaton).findAcceptedWord().orElse(null));
        }
    }


    @Test
    public void testDisproveTokenPassing() {
        CompactNFA<Set<String>> nonInductive = this.trapTokenPassing.getNonInductive(this.rtsTokenPassing);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.trapTokenPassing.getInterpretationAutomaton(this.rtsTokenPassing);
        for(Word<String> origin: Set.of(
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("t", "t", "n"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "t", "t"))) {
                assertNull(this.trapTokenPassing.disprove(this.rtsTokenPassing, origin, origin));
                assertNull(this.trapTokenPassing.disprove(this.rtsTokenPassing, target, target));
                Word<Set<String>> separator = this.trapTokenPassing.disprove(this.rtsTokenPassing, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }

    @Ignore
    @Test
    public void testOutputInductive() throws IOException {
        CompactDFA<Set<String>> inductive = DFAs.minimize(DFAs.complement(
                NFAs.determinize(this.trapTokenPassing.getNonInductive(this.rtsTokenPassing)),
                this.trapTokenPassing.getInterpretationAlphabet(this.rtsTokenPassing)
        ));
        DOTSerializationProvider.getInstance().writeModel(System.out, (Graph<Object, Object>) inductive.graphView());
    }

}