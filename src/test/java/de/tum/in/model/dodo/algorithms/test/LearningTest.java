package de.tum.in.model.dodo.algorithms.test;

import de.tum.in.model.dodo.algorithms.*;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.ListAlphabet;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class LearningTest {
    private RegularTransitionSystem rts;
    private Interpretation<Set<String>> flow;
    private Interpretation<Set<String>> trap;
    private CompactNFA<String> badLanguage;

    @Before
    public void loadRTS() throws IOException {
        Alphabet<String> alphabet = new ListAlphabet<>(List.of("tp", "np", "tq", "nq"));
        this.badLanguage = AutomatonBuilders.newNFA(alphabet)
                // no, or many tokens
                .withInitial(0)
                .from(0)
                .on("np", "nq")
                .to(0)
                .from(0)
                .on("tp", "tq")
                .to(1)
                .from(1)
                .on("np", "nq")
                .to(1)
                .from(1)
                .on("tp", "tq")
                .to(2)
                .from(2)
                .on("tp", "np", "tq", "nq")
                .to(2)
                .withAccepting(0, 2)
                // no p
                .withInitial(3)
                .from(3)
                .on("nq", "tq")
                .to(3)
                .from(3)
                .on("np", "tp")
                .to(4)
                .from(4)
                .on("tp", "np", "tq", "nq")
                .to(4)
                .withAccepting(3)
                .create();

        this.rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing-leader.json")));
        this.flow = new FlowInterpretation(rts);
        this.trap = new TrapInterpretation(rts);
    }


    @Test
    public void learn() {
        assertNotNull(this.rts);
        assertTrue(this.badLanguage.accepts(List.of("tp", "tq", "nq")));
        assertTrue(this.badLanguage.accepts(List.of("np", "nq")));
        assertFalse(this.badLanguage.accepts(List.of("tp", "nq")));
        assertTrue(this.badLanguage.accepts(List.of("tq", "nq")));
        assertThrows(Learning.Unlearnable.class, () -> Learning.learn(rts, List.of(trap), badLanguage));
        assertThrows(Learning.Unlearnable.class, () -> Learning.learn(rts, List.of(flow), badLanguage));
        try {
            assertNotNull(Learning.learn(rts, List.of(trap, flow), badLanguage));
        } catch(Learning.Unlearnable e) {
            fail(e.unremovable.toString());
        }
    }
}