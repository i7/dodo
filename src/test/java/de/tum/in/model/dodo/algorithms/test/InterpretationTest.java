package de.tum.in.model.dodo.algorithms.test;

import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import de.tum.in.model.dodo.algorithms.Interpretation;
import de.tum.in.model.dodo.algorithms.OneShot;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static org.junit.Assert.*;

public class InterpretationTest {
    private Interpretation<Set<String>> trap;

    private RegularTransitionSystem rts;
    @Before
    public void setUp() throws IOException {
        this.rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        this.trap = new Interpretation<>() {
            @Override
            public CompactDFA<LetterPair<String, Set<String>>> getInterpretationAutomaton(RegularTransitionSystem rts) {
                LinkedList<LetterPair<String, Set<String>>> letters = new LinkedList<>();
                for (Set<String> subset : this.getInterpretationAlphabet(rts)) {
                    for (String letter : rts.getInitialer().getInputAlphabet()) {
                        letters.add(LetterPair.of(letter, subset));
                    }
                }
                Alphabet<LetterPair<String, Set<String>>> alphabet = new ListAlphabet<>(letters);
                CompactDFA<LetterPair<String, Set<String>>> result = new CompactDFA<>(alphabet);
                Integer q0 = result.addInitialState();
                Integer q1 = result.addState(true);
                for (LetterPair<String, Set<String>> letter : alphabet) {
                    result.addTransition(q1, letter, q1);
                    result.addTransition(q0, letter, letter.to().contains(letter.from()) ? q1 : q0);
                }
                return result;
            }

            @Override
            public Alphabet<Set<String>> getInterpretationAlphabet(RegularTransitionSystem rts) {
                return new ListAlphabet<>(new LinkedList<>(Sets.powerSet(new HashSet<>(rts.getInitialer().getInputAlphabet()))));
            }

            @Override
            public OneShot oneShot(CompactNFA<String> badWords) {
                throw new RuntimeException("this does not need to be implemented");
            }
        };
    }

    @Test
    public void testNonInductiveTrap() {
        CompactDFA<Set<String>> nonInductive = NFAs.determinize(trap.getNonInductive(rts));
        assertEquals(4, nonInductive.getInputAlphabet().size());
        assertTrue(nonInductive.getInputAlphabet().contains(Set.of()));
        assertTrue(nonInductive.getInputAlphabet().contains(Set.of("n")));
        assertTrue(nonInductive.getInputAlphabet().contains(Set.of("t")));
        assertTrue(nonInductive.getInputAlphabet().contains(Set.of("n", "t")));

        assertFalse(nonInductive.accepts(List.of(Set.of())));
        assertFalse(nonInductive.accepts(List.of(Set.of("n"))));
        assertFalse(nonInductive.accepts(List.of(Set.of("t"))));
        assertFalse(nonInductive.accepts(List.of(Set.of("n", "t"))));

        assertFalse(nonInductive.accepts(List.of(Set.of(), Set.of())));
        assertTrue(nonInductive.accepts(List.of(Set.of(), Set.of("n"))));
        assertTrue(nonInductive.accepts(List.of(Set.of("t"), Set.of())));

        // accepts: ({t} | {})* ({}{n} | {t}{} | {t}{n}) ({t} | {})* (hopefully)
        CompactDFA<Set<String>> expectedResult = NFAs.determinize(AutomatonBuilders.newNFA(nonInductive.getInputAlphabet())
                .withInitial(0)
                .from(0)
                .on(Set.of(), Set.of("t"))
                .to(0)
                .from(0)
                .on(Set.of())
                .to(1)
                .from(0)
                .on(Set.of("t"))
                .to(2)
                .from(1)
                .on(Set.of("n"))
                .to(3)
                .from(2)
                .on(Set.of(), Set.of("n"))
                .to(3)
                .from(3)
                .on(Set.of(), Set.of("t"))
                .loop()
                .withAccepting(3)
                .create());

        CompactDFA<Set<String>> symmetricDifference = DFAs.xor(nonInductive, expectedResult, nonInductive.getInputAlphabet());
        assertTrue(DFAs.acceptsEmptyLanguage(symmetricDifference));
    }

    @Test
    public void testNonAbstractlyReachable() {
        // consider the inductive language {t}*
        CompactNFA<Set<String>> inductive = AutomatonBuilders.newNFA(this.trap.getInterpretationAlphabet(this.rts))
                .withInitial(0)
                .from(0)
                .on(Set.of("t"))
                .loop()
                .withAccepting(0)
                .create();
        CompactNFA<LetterPair<String, String>> abstraction = this.trap.nonAbstractlyReachable(this.rts, inductive, inductive.getInputAlphabet());
        CompactNFA<LetterPair<String, String>> expected = AutomatonBuilders.newNFA(this.rts.getTransducer().getInputAlphabet())
                .withInitial(0)
                .from(0)
                .on(LetterPair.of("n", "n"), LetterPair.of("t", "n"))
                .loop()
                .from(0)
                .on(LetterPair.of("t", "n"))
                .to(1)
                .from(1)
                .on(LetterPair.of("n", "n"), LetterPair.of("t", "n"))
                .loop()
                .withAccepting(1)
                .create();
        CompactDFA<LetterPair<String, String>> symmetricDifference = DFAs.xor(
                NFAs.determinize(abstraction),
                NFAs.determinize(expected),
                this.rts.getTransducer().getInputAlphabet()
        );
        assertTrue(DFAs.acceptsEmptyLanguage(symmetricDifference));
    }

    @Test
    public void testDisproveTwo() {
        Word<String> origin = Word.fromSymbols("t", "n");
        Word<String> target = Word.fromSymbols("t", "t");
        Word<Set<String>> separator = this.trap.disprove(this.rts, origin, target);
        assertNotNull(separator);
        assertEquals(Word.fromSymbols(Set.of("n"), Set.of("n")), separator);
    }

    @Test
    public void testDisproveThree() {
        CompactNFA<Set<String>> nonInductive = this.trap.getNonInductive(this.rts);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.trap.getInterpretationAutomaton(this.rts);
        for(Word<String> origin: Set.of(
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("t", "t", "n"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "t", "t"))) {
                assertNull(this.trap.disprove(this.rts, origin, origin));
                assertNull(this.trap.disprove(this.rts, target, target));
                Word<Set<String>> separator = this.trap.disprove(this.rts, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }

}





























