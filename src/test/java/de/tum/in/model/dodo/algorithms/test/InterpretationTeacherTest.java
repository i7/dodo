package de.tum.in.model.dodo.algorithms.test;

import de.learnlib.algorithms.lstar.dfa.ClassicLStarDFA;
import de.learnlib.util.Experiment;
import de.tum.in.model.dodo.algorithms.InterpretationTeacher;
import de.tum.in.model.dodo.algorithms.TransducerStep;
import de.tum.in.model.dodo.algorithms.TrapInterpretation;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;

import static org.junit.Assert.assertTrue;

public class InterpretationTeacherTest {
    private RegularTransitionSystem rts;
    private InterpretationTeacher teacher;
    private TrapInterpretation trap;
    private CompactNFA<String> badLanguage;
    @Before
    public void setUp() throws Exception {
        this.rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing.json")));
        this.trap = new TrapInterpretation(this.rts);
        this.badLanguage = AutomatonBuilders.newNFA(this.rts.getInitialer().getInputAlphabet())
                .withInitial(0)
                .from(0)
                .on("n", "t")
                .to(0)
                .from(0)
                .on("t")
                .to(1)
                .from(1)
                .on("n", "t")
                .to(1)
                .from(1)
                .on("t")
                .to(2)
                .from(2)
                .on("n", "t")
                .to(2)
                .withAccepting(2)
                .create();
        this.teacher = new InterpretationTeacher<>(
                this.trap,
                this.rts,
                this.badLanguage
        );
    }

    @Test
    public void testTeacher() {
        Experiment.DFAExperiment<Set<String>> experiment = new Experiment.DFAExperiment<>(
                new ClassicLStarDFA<>(this.trap.getInterpretationAlphabet(this.rts), this.teacher),
                this.teacher,
                this.trap.getInterpretationAlphabet(this.rts)
        );
        experiment.run();
        DFA result = experiment.getFinalHypothesis();

        // make sure only inductive statements are recognized by the result:
        CompactDFA<Set<String>> nonInductiveStatements = NFAs.determinize(this.trap.getNonInductive(this.rts));
        CompactDFA<Set<String>> intersection = DFAs.and(result, nonInductiveStatements, this.trap.getInterpretationAlphabet(this.rts));
        assertTrue(DFAs.acceptsEmptyLanguage(intersection));


        // make sure the abstraction is sufficient:
        CompactDFA<LetterPair<String, String>> reachable = DFAs.complement(NFAs.determinize(this.trap.nonAbstractlyReachable(this.rts, result, trap.getInterpretationAlphabet(this.rts))), this.rts.getTransducer().getInputAlphabet());
        CompactDFA<String> fromInitialReachable = NFAs.determinize(TransducerStep.stepAutomaton(NFAs.determinize(this.rts.getInitialer()), reachable));
        CompactDFA<String> reachableBadWords = DFAs.and(fromInitialReachable, NFAs.determinize(this.badLanguage), this.rts.getInitialer().getInputAlphabet());
        assertTrue(DFAs.acceptsEmptyLanguage(reachableBadWords));
    }
}