package de.tum.in.model.dodo.algorithms.test;

import com.google.common.collect.Streams;
import de.tum.in.model.dodo.algorithms.SiphonInterpretation;
import de.tum.in.model.dodo.algorithms.SiphonInterpretationSAT;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class SiphonInterpretationTest {

    SiphonInterpretation siphonTokenPassing;
    SiphonInterpretation siphonTokenPassingNoCache;
    SiphonInterpretation siphonTokenPassingSAT;

    RegularTransitionSystem rtsTokenPassing;

    @Before
    public void setUp() throws IOException {
        this.rtsTokenPassing = RegularTransitionSystem.parseJSON(Files.readString(Path.of("examples", "token-passing-siphons.json")));
        this.siphonTokenPassing = new SiphonInterpretation(this.rtsTokenPassing);
        this.siphonTokenPassingNoCache = new SiphonInterpretation(this.rtsTokenPassing, false);
        this.siphonTokenPassingSAT = new SiphonInterpretationSAT(this.rtsTokenPassing);
    }

    @Test
    public void testDisproveSiphonTest() {
        CompactNFA<Set<String>> nonInductive = this.siphonTokenPassing.getNonInductive(this.rtsTokenPassing);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.siphonTokenPassing.getInterpretationAutomaton(this.rtsTokenPassing);
        for(Word<String> origin: Set.of(Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("t", "t", "t"),
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("n", "t", "n"),
                    Word.fromSymbols("t", "n", "n"),
                    Word.fromSymbols("t", "t", "n"))) {
                assertNull(this.siphonTokenPassing.disprove(this.rtsTokenPassing, origin, origin));
                assertNull(this.siphonTokenPassing.disprove(this.rtsTokenPassing, target, target));
                Word<Set<String>> separator = this.siphonTokenPassing.disprove(this.rtsTokenPassing, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }

    @Test
    public void testDisproveSiphonSATTest() {
        CompactNFA<Set<String>> nonInductive = this.siphonTokenPassingSAT.getNonInductive(this.rtsTokenPassing);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.siphonTokenPassingSAT.getInterpretationAutomaton(this.rtsTokenPassing);
        for(Word<String> origin: Set.of(Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("t", "t", "t"),
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("n", "t", "n"),
                    Word.fromSymbols("t", "n", "n"),
                    Word.fromSymbols("t", "t", "n"))) {
                assertNull(this.siphonTokenPassingSAT.disprove(this.rtsTokenPassing, origin, origin));
                assertNull(this.siphonTokenPassingSAT.disprove(this.rtsTokenPassing, target, target));
                Word<Set<String>> separator = this.siphonTokenPassingSAT.disprove(this.rtsTokenPassing, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }


    private CompactNFA<String> oneWordAutomaton(Word<String> word, Alphabet<String> alphabet) {
        CompactNFA<String> result = new CompactNFA<>(alphabet);
        Integer previous = result.addInitialState();
        for(String letter: word) {
            Integer next = result.addState();
            result.addTransition(previous, letter, next);
            previous = next;
        }
        result.setAccepting(previous, true);
        return result;
    }

    @Test
    public void testOneShot() {
        RegularTransitionSystem rts = this.rtsTokenPassing;
        List<Word<String>> goodWords = List.of(
                Word.fromSymbols("n", "n", "t"),
                Word.fromSymbols("n", "t"),
                Word.fromSymbols("t")
        );
        List<Word<String>> badWords = List.of(
                Word.fromSymbols("t", "n"),
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n"),
                Word.fromSymbols("n", "n"),
                Word.fromSymbols("t", "t"),
                Word.fromSymbols("n", "n", "n"),
                Word.fromSymbols("n", "t", "t"),
                Word.fromSymbols("t", "n", "t"),
                Word.fromSymbols("t", "t", "n"),
                Word.fromSymbols("t", "t", "t")
        );
        for(Word<String> word: goodWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            Word<LetterPair<String, String>> oneShotResult = this.siphonTokenPassing.oneShot(wordAutomaton).findAcceptedWord().orElse(null);
            assertNotNull("one shot falsely disproves " + word, oneShotResult);
            assertEquals(word.length(), oneShotResult.length());
            for(int i=0; i<word.length(); i++) {
                assertEquals(oneShotResult.getSymbol(i).from(), i==(word.length()-1) ? "t" : "n");
                assertEquals(oneShotResult.getSymbol(i).to(), word.getSymbol(i));
            }
        }
        for(Word<String> word: badWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            assertNull("one shot cannot disprove " + word, this.siphonTokenPassing.oneShot(wordAutomaton).findAcceptedWord().orElse(null));
        }
    }

    @Test
    public void testOneShotNoCache() {
        RegularTransitionSystem rts = this.rtsTokenPassing;
        List<Word<String>> goodWords = List.of(
                Word.fromSymbols("n", "n", "t"),
                Word.fromSymbols("n", "t"),
                Word.fromSymbols("t")
        );
        List<Word<String>> badWords = List.of(
                Word.fromSymbols("t", "n"),
                Word.fromSymbols("t", "n", "n"),
                Word.fromSymbols("n", "t", "n"),
                Word.fromSymbols("n"),
                Word.fromSymbols("n", "n"),
                Word.fromSymbols("t", "t"),
                Word.fromSymbols("n", "n", "n"),
                Word.fromSymbols("n", "t", "t"),
                Word.fromSymbols("t", "n", "t"),
                Word.fromSymbols("t", "t", "n"),
                Word.fromSymbols("t", "t", "t")
        );
        for(Word<String> word: goodWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            Word<LetterPair<String, String>> oneShotResult = this.siphonTokenPassingNoCache.oneShot(wordAutomaton).findAcceptedWord().orElse(null);
            assertNotNull("one shot falsely disproves " + word, oneShotResult);
            assertEquals(word.length(), oneShotResult.length());
            for(int i=0; i<word.length(); i++) {
                assertEquals(oneShotResult.getSymbol(i).from(), i==(word.length()-1) ? "t" : "n");
                assertEquals(oneShotResult.getSymbol(i).to(), word.getSymbol(i));
            }
        }
        for(Word<String> word: badWords) {
            CompactNFA<String> wordAutomaton = oneWordAutomaton(word, rts.getInitialer().getInputAlphabet());
            assertTrue(wordAutomaton.accepts(word));
            assertNull("one shot cannot disprove " + word, this.siphonTokenPassingNoCache.oneShot(wordAutomaton).findAcceptedWord().orElse(null));
        }
    }



    @Test
    public void testDisproveTokenPassing() {
        CompactNFA<Set<String>> nonInductive = this.siphonTokenPassing.getNonInductive(this.rtsTokenPassing);
        CompactDFA<LetterPair<String, Set<String>>> interpretation = this.siphonTokenPassing.getInterpretationAutomaton(this.rtsTokenPassing);
        for(Word<String> origin: Set.of(
                Word.fromSymbols("n", "n", "t"))) {
            for (Word<String> target : Set.of(
                    Word.fromSymbols("t", "n", "n"),
                    Word.fromSymbols("n", "t", "n"),
                    Word.fromSymbols("n", "n", "n"),
                    Word.fromSymbols("t", "t", "n"),
                    Word.fromSymbols("t", "n", "t"),
                    Word.fromSymbols("n", "t", "t"),
                    Word.fromSymbols("t", "t", "t"))) {
                assertNull(this.siphonTokenPassing.disprove(this.rtsTokenPassing, origin, origin));
                assertNull(this.siphonTokenPassing.disprove(this.rtsTokenPassing, target, target));
                Word<Set<String>> separator = this.siphonTokenPassing.disprove(this.rtsTokenPassing, origin, target);
                assertNotNull(separator);
                assertFalse(nonInductive.accepts(separator));
                assertTrue(interpretation.accepts(Streams.zip(origin.stream(), separator.stream(), LetterPair::of).toList()));
                assertFalse(interpretation.accepts(Streams.zip(target.stream(), separator.stream(), LetterPair::of).toList()));
            }
        }
    }
}