package de.tum.in.model.dodo.algorithms;

import com.google.common.collect.Sets;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.ListAlphabet;

import java.util.HashSet;
import java.util.Set;

public abstract class SetInterpretation extends Interpretation<Set<String>> {
    protected final RegularTransitionSystem rts;
    protected Alphabet<Set<String>> interpretationAlphabet;
    protected CompactDFA<LetterPair<String, Set<String>>> interpretationAutomaton;
    protected final Set<String> allLetters;

    public SetInterpretation(RegularTransitionSystem rts) {
        this.rts = rts;
        this.interpretationAlphabet = null;
        this.interpretationAutomaton = null;
        this.allLetters = new HashSet<>(this.rts.getInitialer().getInputAlphabet());
    }

    @Override
    public CompactDFA<LetterPair<String, Set<String>>> getInterpretationAutomaton(RegularTransitionSystem rts) {
        assert(this.rts.equals(rts));
        if(this.interpretationAutomaton == null) {
            this.interpretationAutomaton = this.constructInterpretationAutomaton();
        }
        return interpretationAutomaton;
    }

    protected Alphabet<Set<String>> constructInterpretationAlphabet() {
        return new ListAlphabet<>(
                Sets.powerSet(
                        new HashSet<>(this.rts.getInitialer().getInputAlphabet())
                ).stream().toList());
    }

    @Override
    public Alphabet<Set<String>> getInterpretationAlphabet(RegularTransitionSystem rts) {
        assert this.rts.equals(rts);
        if(this.interpretationAlphabet == null) {
            this.interpretationAlphabet = this.constructInterpretationAlphabet();
        }
        return this.interpretationAlphabet;
    }

    abstract protected CompactDFA<LetterPair<String, Set<String>>> constructInterpretationAutomaton();
}
