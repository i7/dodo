package de.tum.in.model.dodo.specification;

public record LetterPair<F, T>(F from, T to) {
    public static <F, T> LetterPair<F, T> of(F from, T to) {
        return new LetterPair<>(from, to);
    }

    public String toString() {
        return "[" + this.from.toString() + "," + this.to.toString() + "]";
    }
}
