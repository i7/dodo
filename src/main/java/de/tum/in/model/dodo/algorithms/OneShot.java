package de.tum.in.model.dodo.algorithms;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;
import com.google.common.collect.Streams;
import de.tum.in.model.dodo.specification.LetterPair;
import net.automatalib.commons.util.Pair;

import java.util.*;
import java.util.function.Predicate;

public abstract class OneShot extends EmptinessGraph<Integer, LetterPair<String, String>> {
    static public class IntegerProxy<S> extends OneShot {
        private final HashBiMap<Integer, S> states = HashBiMap.create();
        private final EmptinessGraph<S, LetterPair<String, String>> proxy;

        public IntegerProxy(EmptinessGraph<S, LetterPair<String, String>> proxy) {
            this.proxy = proxy;
        }

        @Override
        protected boolean isFinal(Integer state) {
            assert(states.containsKey(state));
            return proxy.isFinal(this.states.get(state));
        }

        @Override
        protected Set<Pair<LetterPair<String, String>, Integer>> getTransitions(Integer state) {
            assert(states.containsKey(state));
            Set<Pair<LetterPair<String, String>, S>> transitions = proxy.getTransitions(states.get(state));
            Set<Pair<LetterPair<String, String>, Integer>> result = new HashSet<>();
            for(Pair<LetterPair<String, String>, S> transition: transitions) {
                if(!states.containsValue(transition.getSecond())) {
                    states.put(states.size(), transition.getSecond());
                }
                result.add(Pair.of(transition.getFirst(), states.inverse().get(transition.getSecond())));
            }
            return result;
        }

        @Override
        protected Set<Integer> getInitial() {
            Set<S> initial = proxy.getInitial();
            Set<Integer> result = new HashSet<>(initial.size());
            for(S init: initial) {
                states.put(states.size(), init);
                result.add(states.inverse().get(init));
            }
            return result;
        }
    }
    static public class CompositionalOneShot extends EmptinessGraph<List<Integer>, LetterPair<String, String>> {
        private final List<OneShot> elements;

        public CompositionalOneShot(List<OneShot> elements) {
            assert(elements.size() > 0);
            this.elements = elements;
        }

        @Override
        protected boolean isFinal(List<Integer> state) {
            assert(this.elements.size() == state.size());
            return Streams.zip(this.elements.stream(), state.stream(), OneShot::isFinal).allMatch(Predicate.isEqual(true));
        }

        @Override
        protected Set<Pair<LetterPair<String, String>, List<Integer>>> getTransitions(List<Integer> state) {
            assert(this.elements.size() == state.size());
            List<Map<LetterPair<String, String>, Set<Integer>>> transitions = Streams.zip(this.elements.stream(), state.stream(),
                   (e, s) -> {
                        Map<LetterPair<String, String>, Set<Integer>> result = new HashMap<>();
                        for(Pair<LetterPair<String, String>, Integer> next: e.getTransitions(s)) {
                            if(!result.containsKey(next.getFirst())) {
                                result.put(next.getFirst(), new HashSet<>());
                            }
                            result.get(next.getFirst()).add(next.getSecond());
                        }
                        return result;
                    }).toList();
            Set<LetterPair<String, String>> intersectionKeys = null;
            for(Set<LetterPair<String, String>> keys: transitions.stream().map(Map::keySet).toList()) {
                if(intersectionKeys == null) {
                    intersectionKeys  = new HashSet<>(keys);
                } else {
                    intersectionKeys.retainAll(keys);
                }
            }
            assert(intersectionKeys != null);
            Set<Pair<LetterPair<String, String>, List<Integer>>> result = new HashSet<>();
            for(LetterPair<String, String> key: intersectionKeys) {
                for(List<Integer> next: Sets.cartesianProduct(transitions.stream().map((t) -> t.get(key)).toList())) {
                    result.add(Pair.of(key, next));
                }
            }
            return result;
        }

        @Override
        protected Set<List<Integer>> getInitial() {
            return Sets.cartesianProduct(this.elements.stream().map(OneShot::getInitial).toList());
        }
    }

}
