package de.tum.in.model.dodo.algorithms;

import com.google.common.collect.Sets;
import de.tum.in.model.dodo.specification.LetterPair;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.ts.acceptors.AcceptorTS;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

public class Util {
    public static <I, J> CompactDFA<LetterPair<I, J>> combine(CompactNFA<I> first, CompactDFA<J> second) {
        return combine(NFAs.determinize(first), second);
    }
    public static <I, J> CompactDFA<LetterPair<I, J>> combine(CompactDFA<I> first, CompactNFA<J> second) {
        return combine(first, NFAs.determinize(second));
    }
    public static <I, J> CompactDFA<LetterPair<I, J>> combine(CompactNFA<I> first, CompactNFA<J> second) {
        return combine(NFAs.determinize(first), NFAs.determinize(second));
    }
    public static <I, J> CompactDFA<LetterPair<I, J>> combine(CompactDFA<I> first, CompactDFA<J> second) {
        CompactDFA<LetterPair<I, J>> result = new CompactDFA<>(new ListAlphabet<>(Sets.cartesianProduct(new HashSet<>(first.getInputAlphabet()), new HashSet<>(second.getInputAlphabet())).stream().map((t) -> LetterPair.of((I)t.get(0), (J)t.get(1))).toList()));
        Map<Pair<Integer, Integer>, Integer> states = new HashMap<>();
        Queue<Pair<Integer, Integer>> workList = new LinkedList<>();
        Pair<Integer, Integer> initial = Pair.of(first.getInitialState(), second.getInitialState());
        workList.add(initial);
        states.put(initial, result.addInitialState(first.isAccepting(initial.getFirst()) && second.isAccepting(initial.getSecond())));
        while(!workList.isEmpty()) {
            Pair<Integer, Integer> current = workList.poll();
            for(I firstLetter: first.getLocalInputs(current.getFirst())) {
                for(J secondLetter: second.getLocalInputs(current.getSecond())) {
                    LetterPair<I, J> combined = LetterPair.of(firstLetter, secondLetter);
                    Pair<Integer, Integer> next = Pair.of(first.getSuccessor(current.getFirst(), firstLetter), second.getSuccessor(current.getSecond(), secondLetter));
                    if(!states.containsKey(next)) {
                        workList.add(next);
                        states.put(next, result.addState(first.isAccepting(next.getFirst()) && second.isAccepting(next.getSecond())));
                    }
                    result.addTransition(
                            states.get(current),
                            combined,
                            states.get(next)
                    );
                }
            }
        }
        return result;
    }


    public static <A> Word<A> getAcceptedWord(CompactNFA<A> input) {return getAcceptedWord(input, input.getInputAlphabet());}
    public static <A> Word<A> getAcceptedWord(CompactDFA<A> input) {return getAcceptedWord(input, input.getInputAlphabet());}

    public static <A> Word<A> getAcceptedWord(AcceptorTS<Integer, A> input, Alphabet<A> alphabet) {
        Map<Integer, Word<A>> words = new HashMap<>();
        Queue<Integer> workList = new LinkedList<>();
        for(Integer initial: input.getInitialStates()) {
            words.put(initial, Word.epsilon());
            workList.add(initial);
        }
        while(!(workList.isEmpty())) {
            Integer current = workList.poll();
            if(input.isAccepting(current)) {
                return words.get(current);
            }
            for(A letter: alphabet) {
                for(Integer next: input.getSuccessors(current, letter)) {
                    if (!words.containsKey(next)) {
                        words.put(next, Word.fromWords(words.get(current), Word.fromLetter(letter)));
                        workList.add(next);
                    }
                }
            }
        }
        return null;
    }
}
