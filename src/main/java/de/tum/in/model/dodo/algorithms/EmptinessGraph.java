package de.tum.in.model.dodo.algorithms;

import net.automatalib.commons.util.Pair;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

import java.util.*;

/**
 * Generic class that implements a lazy emptiness check on some automaton. It is used at various points for non-standard
 * product constructions of automata.
 * @param <S> The states of the underlying graph.
 * @param <I> The letters that are read by the automaton the graph encodes.
 */
public abstract class EmptinessGraph<S, I> {

    public long getExploredStates() {
        return exploredStates;
    }

    private long exploredStates;
    /**
     * Computationally determines whether a given state is accepting.
     * @param state For this state the acceptance condition is queried.
     * @return true if the state is accepting otherwise false.
     */
    abstract protected boolean isFinal(S state);

    /**
     * Computationally encode the transition relation. This is queried at most once per state.
     * @param state For this state the successor states are queried.
     * @return A set of pairs of letters and states that are part of the transitions of the underlying graph.
     */
    abstract protected Set<Pair<I, S>> getTransitions(S state);

    /**
     * Computationally encode the initial states.
     * @return The initial state of the graph.
     */
    abstract protected Set<S> getInitial();

    /**
     * The actual search for an accepted word.
     * @return Optional.empty() if the language of the graph is empty otherwise Optional.of(w) where w is an accepted
     * word.
     */
    public Optional<Word<I>> findAcceptedWord() {
        Map<S, Word<I>> words = new HashMap<>();
        Queue<S> workQueue = new ArrayDeque<>();
        this.exploredStates = 0;
        for(S initial: this.getInitial()) {
            workQueue.add(initial);
            words.put(initial, Word.epsilon());
        }

        while(!workQueue.isEmpty()) {
            S currentState = workQueue.poll();
            this.exploredStates++;
            Word<I> currentWord = words.get(currentState);
            WordBuilder<I> prolongation = null;
            for(Pair<I, S> transition: this.getTransitions(currentState)) {
                S newSuccessor = transition.getSecond();
                I input = transition.getFirst();
                if(!words.containsKey(newSuccessor)) {
                    if(prolongation == null) {
                        prolongation = new WordBuilder<>(currentWord);
                        prolongation.append(input);
                    } else {
                        prolongation.set(currentWord.length(), input);
                    }
                    if(this.isFinal(newSuccessor)) {
                        return Optional.of(prolongation.toWord());
                    } else {
                        words.put(newSuccessor, prolongation.toWord());
                    }
                    workQueue.add(newSuccessor);
                }
            }
        }
        return Optional.empty();
    }
}
