package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.words.Word;

import java.util.Set;

public class NoOpAbstractor<G> extends Abstractor<G> {
    public NoOpAbstractor(RegularTransitionSystem rts, Interpretation<G> interpretation) {
        super(rts, interpretation);
    }

    @Override
    protected CompactDFA<G> getLanguage(Word<G> word) {
        return null;
    }
    static public <G> NoOpAbstractor<G> create(RegularTransitionSystem rts, Interpretation<G> interpretation) {
        return new NoOpAbstractor<>(rts, interpretation);
    }

}
