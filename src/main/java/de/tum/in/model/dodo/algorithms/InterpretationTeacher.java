package de.tum.in.model.dodo.algorithms;

import de.learnlib.api.oracle.EquivalenceOracle;
import de.learnlib.api.oracle.MembershipOracle;
import de.learnlib.api.query.DefaultQuery;
import de.learnlib.api.query.Query;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.*;

public class InterpretationTeacher<G> implements MembershipOracle.DFAMembershipOracle<G>, EquivalenceOracle.DFAEquivalenceOracle<G> {
    private final Interpretation<G> interpretation;
    private final RegularTransitionSystem rts;
    private final CompactDFA<LetterPair<String, String>> initialToBad;

    public InterpretationTeacher(Interpretation<G> interpretation, RegularTransitionSystem rts, CompactNFA<String> badLanguage) {
        this.interpretation = interpretation;
        this.rts = rts;
        this.initialToBad = Util.combine(this.getRts().getInitialer(), badLanguage);
    }

    public Interpretation<G> getInterpretation() {
        return this.interpretation;
    }


    @Override
    public void processQueries(Collection<? extends Query<G, Boolean>> queries) {
        for(Query<G, Boolean> query: queries) {
            query.answer(!this.getInterpretation().isNonInductive(this.getRts(), query.getInput()));
        }
    }


    static public <G> Word<LetterPair<String, String>> findBadWord(RegularTransitionSystem rts, CompactDFA<LetterPair<String, String>> initialToBad, DFA<?, G> hypothesis, Interpretation<G> interpretation, Collection<? extends G> inputs) {
        CompactDFA<LetterPair<String, String>> abstractlyReach = initialToBad;
            if(Util.getAcceptedWord(abstractlyReach) == null) {
                return null;
            }
            CompactDFA<LetterPair<String, String>> nonAbstractlyReachable = NFAs.determinize(interpretation.nonAbstractlyReachable(rts, hypothesis, inputs));
            CompactDFA<LetterPair<String, String>> abstractlyReachable = DFAs.complement(nonAbstractlyReachable, nonAbstractlyReachable.getInputAlphabet());
            abstractlyReach = DFAs.and(abstractlyReach, abstractlyReachable, abstractlyReachable.getInputAlphabet());
        return Util.getAcceptedWord(abstractlyReach);
    }


    private <S> Word<LetterPair<String, String>> findBadWord(DFA<S, G> hypothesis, Collection<? extends G> inputs) {
        return findBadWord(this.getRts(), this.initialToBad, hypothesis, this.interpretation, inputs);
    }


    @Override
    public @Nullable DefaultQuery<G, Boolean> findCounterExample(DFA<?, G> hypothesis, Collection<? extends G> inputs) {
        // make sure the hypothesis only accepts inductive statements
        Word<G> counterExample = this.interpretation.findInductiveCounterExample(this.rts, hypothesis, inputs);
        if(counterExample != null) return new DefaultQuery<>(counterExample, false);
        // check whether the hypothesis already is enough
        Word<LetterPair<String, String>> badWord = this.findBadWord(hypothesis, inputs);
        if(badWord == null) return null;
        counterExample = this.interpretation.disprove(this.getRts(), badWord);
        if(counterExample != null) {
            assert this.interpretation.accepts(this.getRts(), badWord.transform(LetterPair::from), counterExample);
            assert !this.interpretation.accepts(this.getRts(), badWord.transform(LetterPair::to), counterExample);
            assert !hypothesis.accepts(counterExample);
            return new DefaultQuery<>(counterExample, true);
        }
        throw new RuntimeException("cannot refine further");
    }

    public RegularTransitionSystem getRts() {
        return rts;
    }
}
