package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public abstract class Abstractor<G> {
    record WordAbstraction<G>(Word<G> word, CompactDFA<G> language) {}

    protected final RegularTransitionSystem rts;
    protected final Interpretation<G> interpretation;
    private final List<WordAbstraction<G>> words;

    public CompactDFA<LetterPair<String, String>> getAbstraction() {
        return abstraction;
    }

    private CompactDFA<LetterPair<String, String>> abstraction;

    public Abstractor(RegularTransitionSystem rts, Interpretation<G> interpretation) {
        this.rts = rts;
        this.interpretation = interpretation;
        this.words = new LinkedList<>();
        this.abstraction = null;
    }

    int accumulatedSize() {
        int result = 0;
        for(WordAbstraction<G> abstraction: this.words) {
            result += abstraction.language.size();
        }
        return result;
    }

    public boolean generalize(Word<G> word) {
        CompactDFA<G> language = this.getLanguage(word);
        if(language == null) {
            return false;
        }
        this.words.add(new WordAbstraction(word, language));
        CompactDFA<LetterPair<String, String>> nonAbstraction = NFAs.determinize(this.interpretation.nonAbstractlyReachable(this.rts, language, language.getInputAlphabet()));
        CompactDFA<LetterPair<String, String>> abstraction = DFAs.complement(nonAbstraction, nonAbstraction.getInputAlphabet());
        if(this.abstraction == null) {
            this.abstraction = abstraction;
        } else {
            this.abstraction = DFAs.and(this.abstraction, abstraction, abstraction.getInputAlphabet());
        }
        return true;
    }

    abstract protected CompactDFA<G> getLanguage(Word<G> word);

}
