package de.tum.in.model.dodo.algorithms;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

public class CrowdAbstractor<G> extends Abstractor<G> {
    private final int k;
    public CrowdAbstractor(RegularTransitionSystem rts, Interpretation<G> interpretation, int k) {
        super(rts, interpretation);
        this.k = k;
    }

    @Override
    protected CompactDFA<G> getLanguage(Word<G> word) {
        Alphabet<G> letters = new ListAlphabet<>(word.stream().distinct().toList());
        Multiset<G> counter = HashMultiset.create();
        for(G letter: word) {
            if(letter.equals(this.rts.getSigma())) return null;
            if(counter.count(letter) < this.k) {
                counter.add(letter);
            }
        }
        CompactDFA<G> result = new CompactDFA<>(letters);
        Map<Multiset<G>, Integer> states = new HashMap<>();
        Multiset<G> initial = HashMultiset.create();
        states.put(initial, result.addInitialState(initial.equals(counter)));
        Queue<Multiset<G>> workList = new LinkedList<>();
        workList.add(initial);
        while(!workList.isEmpty()) {
            Multiset<G> current = workList.poll();
            for(G letter: letters) {
                Multiset<G> next = HashMultiset.create(current);
                if(next.count(letter) < this.k) {
                    next.add(letter);
                }
                if(!states.containsKey(next)) {
                    Integer state = result.addState(next.equals(counter));
                    states.put(next, state);
                    workList.add(next);
                }
                result.addTransition(states.get(current), letter, states.get(next));
            }
        }
        return result;
    }
}
