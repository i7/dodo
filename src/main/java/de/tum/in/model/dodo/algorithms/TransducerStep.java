package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

public class TransducerStep {
    static public <S, G> CompactNFA<G> stepAutomaton(CompactDFA<S> source, CompactDFA<LetterPair<S, G>> step) {
        Set<G> symbols = new HashSet<>();
        for(LetterPair<S, G> stepInput: step.getInputAlphabet()) {
            if(source.getInputAlphabet().contains(stepInput.from())) symbols.add(stepInput.to());
        }
        CompactNFA<G> result = new CompactNFA<>(new ListAlphabet<>(symbols.stream().toList()));
        Map<Pair<Integer, Integer>, Integer> states = new HashMap<>();
        Queue<Pair<Integer, Integer>> workLine = new LinkedList<>();
        Pair<Integer, Integer> initialState = Pair.of(source.getInitialState(), step.getInitialState());
        workLine.add(initialState);
        states.put(initialState, result.addInitialState(source.isAccepting(source.getInitialState())  && step.isAccepting(step.getInitialState())));
        while(!workLine.isEmpty()) {
            Pair<Integer, Integer> state = workLine.poll();
            for(S letter: source.getLocalInputs(state.getFirst())) {
                for(LetterPair<S, G> stepLetter: step.getLocalInputs(state.getSecond())) {
                    if(!letter.equals(stepLetter.from())) continue;
                    Integer nextSource = source.getSuccessor(state.getFirst(), letter);
                    Integer nextStep = step.getSuccessor(state.getSecond(), stepLetter);
                    Pair<Integer, Integer> next = Pair.of(nextSource, nextStep);
                    if(!states.containsKey(next)) {
                        states.put(next, result.addState(source.isAccepting(nextSource) && step.isAccepting(nextStep)));
                        workLine.add(next);
                    }
                    result.addTransition(
                            states.get(state),
                            stepLetter.to(),
                            states.get(next)
                    );
                }
            }
        }
        return result;
    }

    static public <S, G> CompactNFA<G> stepAutomaton(CompactNFA<S> source, CompactDFA<LetterPair<S, G>> step) {
        Set<G> symbols = new HashSet<>();
        for(LetterPair<S, G> stepInput: step.getInputAlphabet()) {
            if(source.getInputAlphabet().contains(stepInput.from())) symbols.add(stepInput.to());
        }
        CompactNFA<G> result = new CompactNFA<>(new ListAlphabet<>(symbols.stream().toList()));
        Map<Pair<Integer, Integer>, Integer> states = new HashMap<>();
        Queue<Pair<Integer, Integer>> workLine = new LinkedList<>();
        for(Integer initialSource: source.getInitialStates()) {
            Pair<Integer, Integer> state = Pair.of(initialSource, step.getInitialState());
            workLine.add(state);
            states.put(state, result.addInitialState(source.isAccepting(initialSource)  && step.isAccepting(step.getInitialState())));
        }
        while(!workLine.isEmpty()) {
            Pair<Integer, Integer> state = workLine.poll();
            for(S letter: source.getLocalInputs(state.getFirst())) {
                for(LetterPair<S, G> stepLetter: step.getLocalInputs(state.getSecond())) {
                    if(!letter.equals(stepLetter.from())) continue;
                    for(Integer nextSource: source.getSuccessors(state.getFirst(), letter)) {
                        Integer nextStep = step.getSuccessor(state.getSecond(), stepLetter);
                        Pair<Integer, Integer> next = Pair.of(nextSource, nextStep);
                        if(!states.containsKey(next)) {
                            states.put(next, result.addState(source.isAccepting(nextSource)  && step.isAccepting(nextStep)));
                            workLine.add(next);
                        }
                        result.addTransition(
                                states.get(state),
                                stepLetter.to(),
                                states.get(next)
                        );
                    }
                }
            }
        }
        return result;
    }

    static public <S, G> CompactNFA<G> stepAutomaton(CompactNFA<S> source, CompactNFA<LetterPair<S, G>> step) {
        Set<G> symbols = new HashSet<>();
        for(LetterPair<S, G> stepInput: step.getInputAlphabet()) {
            if(source.getInputAlphabet().contains(stepInput.from())) symbols.add(stepInput.to());
        }
        CompactNFA<G> result = new CompactNFA<>(new ListAlphabet<>(symbols.stream().toList()));
        Map<Pair<Integer, Integer>, Integer> states = new HashMap<>();
        Queue<Pair<Integer, Integer>> workLine = new LinkedList<>();
        for(Integer initialSource: source.getInitialStates()) {
            for(Integer initialStep: step.getInitialStates()) {
                Pair<Integer, Integer> state = Pair.of(initialSource, initialStep);
                workLine.add(state);
                states.put(state, result.addInitialState(source.isAccepting(initialSource)  && step.isAccepting(initialStep)));
            }
        }
        while(!workLine.isEmpty()) {
            Pair<Integer, Integer> state = workLine.poll();
            for(S letter: source.getLocalInputs(state.getFirst())) {
                for(LetterPair<S, G> stepLetter: step.getLocalInputs(state.getSecond())) {
                    if(!letter.equals(stepLetter.from())) continue;
                    for(Integer nextSource: source.getSuccessors(state.getFirst(), letter)) {
                        for(Integer nextStep: step.getSuccessors(state.getSecond(), stepLetter)) {
                            Pair<Integer, Integer> next = Pair.of(nextSource, nextStep);
                            if(!states.containsKey(next)) {
                                states.put(next, result.addState(source.isAccepting(nextSource)  && step.isAccepting(nextStep)));
                                workLine.add(next);
                            }
                            result.addTransition(
                                    states.get(state),
                                    stepLetter.to(),
                                    states.get(next)
                            );
                        }
                    }
                }
            }
        }
        return result;
    }
}
