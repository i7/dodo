package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.stream.Collectors;

import net.automatalib.commons.util.Pair;
import net.automatalib.ts.acceptors.AcceptorTS;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.DimacsOutputSolver;

public class FlowInterpretation extends SetInterpretation {


    public FlowInterpretation(RegularTransitionSystem rts) {
        super(rts);
    }

    @Override
    public boolean accepts(RegularTransitionSystem rts, Word<String> word, Word<Set<String>> statement) {
        assert word.length() == statement.length();
        boolean intersects = false;
        for(int i=0; i<word.length(); i++) {
            String wordLetter = word.getSymbol(i);
            Set<String> statementLetter = statement.getSymbol(i);
            if(statementLetter.contains(wordLetter)) {
                if (intersects) {
                    return false;
                } else {
                    intersects = true;
                }
            }
        }
        return intersects;
    }

    @Override
    protected CompactDFA<LetterPair<String, Set<String>>> constructInterpretationAutomaton() {
        LinkedList<LetterPair<String, Set<String>>> letters = new LinkedList<>();
        for (String s : this.rts.getInitialer().getInputAlphabet()) {
            for (Set<String> l : this.interpretationAlphabet) {
                letters.add(LetterPair.of(s, l));
            }
        }
        Alphabet<LetterPair<String, Set<String>>> alphabet = new ListAlphabet<>(letters);
        CompactDFA<LetterPair<String, Set<String>>> result = new CompactDFA<>(alphabet);
        Integer first = result.addInitialState(false);
        Integer second = result.addState(true);
        Integer third = result.addState(false);
        for(LetterPair<String, Set<String>> l: letters) {
            if(l.to().contains(l.from())) {
                result.addTransition(first, l, second);
                result.addTransition(second, l, third);
            } else {
                result.addTransition(first, l, first);
                result.addTransition(second, l, second);
            }
            result.addTransition(third, l, third);
        }
        return result;
    }

    @Override
    public OneShot oneShot(CompactNFA<String> badWords) {
        record State(Integer initial, Set<Integer> abstraction, Integer bad) {}
        EmptinessGraph<State, LetterPair<String, String>> result = new EmptinessGraph<>() {
            private final CompactNFA<LetterPair<String, String>> nonAbstractly = nonAbstractlyReachable(rts, DFAs.complement(NFAs.determinize(getNonInductive(rts), getInterpretationAlphabet(rts)), getInterpretationAlphabet(rts)), getInterpretationAlphabet(rts));
            private final CompactNFA<String> initial = rts.getInitialer();
            @Override
            protected boolean isFinal(State state) {
                return badWords.isAccepting(state.bad)
                        && initial.isAccepting(state.initial)
                        && state.abstraction.stream().noneMatch(nonAbstractly::isAccepting);
            }

            @Override
            protected Set<Pair<LetterPair<String, String>, State>> getTransitions(State state) {
                Set<Pair<LetterPair<String, String>, State>> result = new HashSet<>();
                for(String badLetter: badWords.getLocalInputs(state.bad)) {
                    for(String initialLetter: initial.getLocalInputs(state.initial)) {
                        LetterPair<String, String> letter = LetterPair.of(initialLetter, badLetter);
                        for(Integer nextBad: badWords.getSuccessors(state.bad, badLetter)) {
                            for(Integer nextInitial: initial.getSuccessors(state.initial, initialLetter)) {
                                Set<Integer> nextAbstraction = new HashSet<>();
                                for(Integer abstraction: state.abstraction) {
                                    nextAbstraction.addAll(nonAbstractly.getSuccessors(abstraction, letter));
                                }
                                result.add(Pair.of(letter, new State(nextInitial, nextAbstraction, nextBad)));
                            }
                        }
                    }
                }
                return result;
            }

            @Override
            protected Set<State> getInitial() {
                Set<State> result = new HashSet<>();
                for(Integer initial: initial.getInitialStates()) {
                    for(Integer bad: badWords.getInitialStates()) {
                        result.add(new State(initial, nonAbstractly.getInitialStates(), bad));
                    }
                }
                return result;
            }
        };
        return new OneShot.IntegerProxy<>(result);
    }

    @Override
    public Word<Set<String>> disprove(RegularTransitionSystem rts, Word<String> source, Word<String> target) {
        assert source.length() == target.length();
        if(source.equals(target)) return null;
        class Indices {
            private final Map<String, Integer> alphabet;
            private final int alphabetSize;
            private final int n;
            private final ISolver solver;
            private final DimacsOutputSolver output;
            private final CompactNFA<LetterPair<String, String>> transducer;

            public Indices() {
                this.alphabet = new HashMap<>();
                this.alphabetSize = allLetters.size();
                for(String sigma: allLetters) {
                    this.alphabet.put(sigma, this.alphabet.keySet().size());
                }
                this.n = source.length();
                this.solver = SolverFactory.newDefault();
                this.output = new DimacsOutputSolver(new PrintWriter(new StringWriter()));
                this.transducer = rts.getTransducer();
            }

            public int getLetter(String letter, int position) {
                assert this.alphabet.containsKey(letter);
                assert 0 < position && position <= this.n;
                return (position-1)*this.alphabetSize + this.alphabet.get(letter) + 1;
            }

            public int getState(Integer state, int position, boolean presetIntersected, int postSetIntersection) {
                assert 0 <= state && state < this.transducer.size();
                assert 0 <= position && position <= this.n;
                assert 0 <= postSetIntersection  && postSetIntersection <= 2;
                return (this.n*this.alphabetSize) + (position*this.transducer.size()*6) + 6*state + 3*(presetIntersected ? 0 : 1) + postSetIntersection + 1;
            }

            public int getExclusionVariable() {
                return (this.n*this.alphabetSize) + ((this.n+1)*6*this.transducer.size()) + 1;
            }

            public void addInitialClauses() {
                // mark initial states of transducer without intersection yet
                for(Integer state: this.transducer.getInitialStates()) {
                    try {
                        this.solver.addClause(new VecInt(new int[]{this.getState(state, 0, false, 0)}));
                    } catch(ContradictionException e){
                        throw new RuntimeException("Error in propositional encoding for flow: mark initial states of transducer");
                    }
                }
            }

            public void addFinalClauses(VecInt includeSource) {
                try {
                    this.solver.addExactly(includeSource, 1);
                    this.output.addExactly(includeSource, 1);
                } catch(ContradictionException e) {
                    throw new RuntimeException("Error in propositional encoding for flow: intersection with source");
                }
                // mark no final states of the transducer with intersection
                for(Integer state: this.transducer.getStates()) {
                    if(!this.transducer.isAccepting(state)) continue;
                    try {
                        this.solver.addClause(new VecInt(new int[]{-this.getState(state, this.n, true, 0)}));
                        this.output.addClause(new VecInt(new int[]{-this.getState(state, this.n, true, 0)}));
                        this.solver.addClause(new VecInt(new int[]{-this.getState(state, this.n, true, 2)}));
                        this.output.addClause(new VecInt(new int[]{-this.getState(state, this.n, true, 2)}));
                    } catch(ContradictionException e) {
                        throw new RuntimeException("Error in propositional encoding for flow: not mark final states of transducer");
                    }
                }
            }

            public void addClausesForStep(int step, VecInt includesSource) {
                if(0 < step) {
                    try {
                        // word excludes target
                        // by no intersection
                        this.solver.addClause(new VecInt(new int[]{-this.getLetter(target.getSymbol(step - 1), step), -this.getExclusionVariable()}));
                        this.output.addClause(new VecInt(new int[]{-this.getLetter(target.getSymbol(step - 1), step), -this.getExclusionVariable()}));
                        // by double intersection
                        VecInt doubleIntersection = new VecInt(new int[]{-this.getLetter(target.getSymbol(step - 1), step), this.getExclusionVariable()});
                        for(int s = 1; s <= target.length(); s++) {
                            if(s == step) continue;
                            doubleIntersection.push(this.getLetter(target.getSymbol(s - 1), s));
                        }
                        this.solver.addClause(doubleIntersection);
                        this.output.addClause(doubleIntersection);
                        // word includes source
                        includesSource.push(this.getLetter(source.getSymbol(step-1), step));
                    } catch(ContradictionException e) {
                        throw new RuntimeException("Error in propositional encoding for flow: source & target in step " + step);
                    }
                }
                if(step < this.n) {
                    // inductive check
                    for(Integer state: this.transducer.getStates()) {
                        for(LetterPair<String, String> input: this.transducer.getLocalInputs(state)) {
                            for(Integer next: this.transducer.getSuccessors(state, input)) {
                                try {
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 0) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 0) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            -this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 0) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            -this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 0) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            -this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            -this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 2),
                                            this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, false, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 2),
                                            this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, false, 2) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 2),
                                            -this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 2),
                                            -this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            -this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 0),
                                            -this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false, 2) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            -this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false, 1),
                                            -this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    // initial word already intersected
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 0),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 0) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 0),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 0) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 1),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 1),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 2),
                                            this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 2),
                                            this.getLetter(input.from(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 0),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 0),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 1) }));
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 1),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                    this.output.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, true, 1),
                                            this.getLetter(input.from(), step+1),
                                            -this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true, 2) }));
                                } catch(ContradictionException e) {
                                    throw new RuntimeException("Error in propositional encoding for flow: check for flow in step " + step);
                                }
                            }
                        }
                    }
                }
            }

            public Word<Set<String>> extractResult() {
                try {
                    if (!this.solver.isSatisfiable()) {
                        return null;
                    }
                } catch(TimeoutException e) {
                    return null;
                }
                List<Set<String>> result = new LinkedList<>();
                for(int i = 1; i <= this.n; i++) {
                    final int position = i;
                    result.add(
                            allLetters.stream().filter((x) -> this.solver.model(this.getLetter(x, position))).collect(Collectors.toSet())
                    );
                }
                return Word.fromList(result);
            }
        }

        Indices indices = new Indices();
        indices.addInitialClauses();
        VecInt includesSource = new VecInt();
        for(int i = 0; i <= source.length(); i++) {
            indices.addClausesForStep(i, includesSource);
        }
        indices.addFinalClauses(includesSource);
        return indices.extractResult();
    }


    @Override
    public boolean isNonInductive(RegularTransitionSystem rts, Word<Set<String>> statement) {
        record State(Integer transducer, int length, int intersectedBefore, int intersectedAfter) {}
        EmptinessGraph<State, LetterPair<String, String>> findViolation = new EmptinessGraph<>() {
            @Override
            protected boolean isFinal(State state) {
                return rts.getTransducer().isAccepting(state.transducer) && state.length == statement.length() && state.intersectedBefore == 1 && state.intersectedAfter != 1;
            }

            @Override
            protected Set<Pair<LetterPair<String, String>, State>> getTransitions(State state) {
                Set<Pair<LetterPair<String, String>, State>> result = new HashSet<>();
                if(state.length == statement.length()) {
                    return result;
                }
                Set<String> statementLetter = statement.getSymbol(state.length);
                for(LetterPair<String, String> input: rts.getTransducer().getLocalInputs(state.transducer)) {
                    for(Integer nextTransducer: rts.getTransducer().getSuccessors(state.transducer, input)) {
                        State next = new State(nextTransducer, state.length+1,
                                statementLetter.contains(input.from()) ? Math.min(state.intersectedBefore+1, 2) : state.intersectedBefore,
                                statementLetter.contains(input.to()) ? Math.min(state.intersectedAfter+1, 2) : state.intersectedAfter
                        );
                        if(next.intersectedBefore == 2) {
                            continue;
                        }
                        result.add(Pair.of(input, next));
                    }
                }
                return result;
            }

            @Override
            protected Set<State> getInitial() {
                Set<State> result = new HashSet<>();
                for(Integer initial: rts.getTransducer().getInitialStates()) {
                    result.add(new State(initial, 0, 0, 0));
                }
                return result;
            }
        };
        return findViolation.findAcceptedWord().isPresent();
    }

    @Override
    public <S> Word<Set<String>> findInductiveCounterExample(RegularTransitionSystem rts, DFA<S, Set<String>> hypothesis, Collection<? extends Set<String>> hypothesisAlphabet) {
        record  State<S>(Integer transducer, int intersectedBefore, int intersectedAfter, S hypothesisState) {}
        EmptinessGraph<State<S>, Set<String>> findViolation = new EmptinessGraph<>() {
            @Override
            protected boolean isFinal(State<S> state) {
                return rts.getTransducer().isAccepting(state.transducer) && state.intersectedBefore == 1 && state.intersectedAfter != 1 && hypothesis.isAccepting(state.hypothesisState);
            }

            @Override
            protected Set<Pair<Set<String>, State<S>>> getTransitions(State state) {
                Set<Pair<Set<String>, State<S>>> result = new HashSet<>();
                for(LetterPair<String, String> transducerLetter: rts.getTransducer().getLocalInputs(state.transducer)) {
                    for (Set<String> letter : hypothesisAlphabet) {
                        if(letter.contains(transducerLetter.to())) {
                            continue;
                        }
                        S nextHypothesis = hypothesis.getSuccessor((S)state.hypothesisState, letter);
                        if(nextHypothesis == null) {
                            continue;
                        }
                        for(Integer nextTransducer: rts.getTransducer().getSuccessors(state.transducer, transducerLetter)) {
                            State<S> next = new State<>(
                                    nextTransducer,
                                    letter.contains(transducerLetter.from()) ? Math.min(state.intersectedBefore+1, 2) : state.intersectedBefore,
                                    letter.contains(transducerLetter.to()) ? Math.min(state.intersectedAfter+1, 2) : state.intersectedAfter,
                                    nextHypothesis);
                            if(next.intersectedBefore == 2) {
                                continue;
                            }
                            result.add(Pair.of(letter, next));
                        }
                    }
                }
                return result;
            }

            @Override
            protected Set<State<S>> getInitial() {
                Set<State<S>> result = new HashSet<>();
                for(Integer initialTransducer: rts.getTransducer().getInitialStates()) {
                    for(S initialHypothesis: hypothesis.getInitialStates()) {
                        result.add(new State<>(initialTransducer, 0, 0, initialHypothesis));
                    }
                }
                return result;
            }
        };
        return findViolation.findAcceptedWord().orElse(null);
    }

    @Override
    public <S> CompactNFA<LetterPair<String, String>> nonAbstractlyReachable(RegularTransitionSystem rts, AcceptorTS<S, Set<String>> inductiveStatements, Collection<? extends Set<String>> inputs) {
        record State<S>(Integer intersectedBefore, S statements, Integer intersectedAfter) {}
        CompactNFA<LetterPair<String, String>> result = new CompactNFA<>(rts.getTransducer().getInputAlphabet());
        Map<State<S>, Integer> states = new HashMap<>();
        Queue<State<S>> workList = new LinkedList<>();
        for(S statements: inductiveStatements.getInitialStates()) {
            State<S> state = new State<>(0, statements, 0);
            workList.add(state);
            states.put(state, result.addInitialState(false));
        }
        while(!workList.isEmpty()) {
            State<S> state = workList.poll();
            for(LetterPair<String, String> letter: result.getInputAlphabet()) {
                for(Set<String> statementLetter: inputs) {
                    for(S nextStatements: inductiveStatements.getSuccessors(state.statements, statementLetter)) {
                        State<S> next = new State<>(
                                statementLetter.contains(letter.from()) ? Math.min(state.intersectedBefore+1, 2) : state.intersectedBefore,
                                nextStatements,
                                statementLetter.contains(letter.to()) ? Math.min(state.intersectedAfter+1, 2) : state.intersectedAfter
                        );
                        if(next.intersectedBefore == 2) {
                            continue;
                        }
                        if(!states.containsKey(next)) {
                            workList.add(next);
                            states.put(next, result.addState(
                                    next.intersectedBefore == 1
                                            && inductiveStatements.isAccepting(next.statements)
                                            && next.intersectedAfter != 1));
                        }
                        result.addTransition(
                                states.get(state),
                                letter,
                                states.get(next));
                    }
                }
            }
        }
        return result;
    }
}
