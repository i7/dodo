package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

public class BowAbstractor<G> extends Abstractor<G> {
    public BowAbstractor(RegularTransitionSystem rts, Interpretation<G> interpretation) {
        super(rts, interpretation);
    }


    @Override
    protected CompactDFA<G> getLanguage(Word<G> word) {
        Alphabet<G> letters = new ListAlphabet<>(word.stream().distinct().toList());
        if(word.isEmpty() || word.length() == 1 || word.stream().anyMatch(this.rts.getSigma()::equals)) {
            return null;
        }
        Set<Pair<G, G>> validPairs = new HashSet<>();
        Pair<G, G> firstPair = Pair.of(word.firstSymbol(), word.getSymbol(1));
        Pair<G, G> lastPair = Pair.of(word.lastSymbol(), word.firstSymbol());
        Set<G> alphabet = new HashSet<>();
        alphabet.add(word.firstSymbol());
        for(int i = 1; i<word.length()-1; i++) {
            alphabet.add(word.getSymbol(i));
            validPairs.add(Pair.of(word.getSymbol(i), word.getSymbol(i+1)));
        }
        alphabet.add(word.lastSymbol());
        Map<Pair<G, G>, Integer> states = new HashMap<>();
        CompactDFA<G> result = new CompactDFA<>(letters);
        states.put(Pair.of(null, null), result.addInitialState());
        states.put(Pair.of(firstPair.getFirst(), null), result.addState(false));
        states.put(firstPair, result.addState(lastPair.equals(Pair.of(firstPair.getSecond(), firstPair.getFirst()))));
        result.addTransition(states.get(Pair.of(null, null)), firstPair.getFirst(), states.get(Pair.of(firstPair.getFirst(), null)));
        result.addTransition(states.get(Pair.of(firstPair.getFirst(), null)), firstPair.getSecond(), states.get(firstPair));
        Queue<Pair<G, G>> workList = new LinkedList<>();
        workList.add(firstPair);
        while(!workList.isEmpty()) {
            Pair<G, G> current = workList.poll();
            for(G letter: alphabet) {
                if(!validPairs.contains(Pair.of(current.getSecond(), letter))) continue;
                Pair<G, G> next = Pair.of(current.getFirst(), letter);
                if(!states.containsKey(next)) {
                    states.put(next, result.addState(lastPair.equals(Pair.of(next.getSecond(), next.getFirst()))));
                    workList.add(next);
                }
                result.addTransition(states.get(current), letter, states.get(next));
            }
        }
        return result;
    }
}
