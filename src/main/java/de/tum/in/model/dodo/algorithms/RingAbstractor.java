package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;

public class RingAbstractor<G> extends Abstractor<G> {
    public RingAbstractor(RegularTransitionSystem rts, Interpretation<G> interpretation) {
        super(rts, interpretation);
    }

    @Override
    protected CompactDFA<G> getLanguage(Word<G> word) {
        Alphabet<G> letters = new ListAlphabet<>(word.stream().distinct().toList());
        if(word.isEmpty() || word.length() == 1 || word.stream().anyMatch(this.rts.getSigma()::equals)) {
            return null;
        }
        Set<Pair<G, G>> validPairs = new HashSet<>();
        validPairs.add(Pair.of(word.lastSymbol(), word.firstSymbol()));
        Set<G> alphabet = new HashSet<>();
        for(int i = 0; i<word.length()-1; i++) {
            alphabet.add(word.getSymbol(i));
            validPairs.add(Pair.of(word.getSymbol(i), word.getSymbol(i+1)));
        }
        alphabet.add(word.lastSymbol());
        Map<Pair<G, G>, Integer> states = new HashMap<>();
        CompactDFA<G> result = new CompactDFA<>(letters);
        states.put(Pair.of(null, null), result.addInitialState());
        for(G letter: alphabet) {
            Pair<G, G> origin = Pair.of(letter, null);
            states.put(origin, result.addState());
            result.addTransition(states.get(Pair.of(null, null)), letter, states.get(Pair.of(letter, null)));
            for(G target: alphabet) {
                Pair<G, G> next = Pair.of(letter, target);
                if(!validPairs.contains(next)) continue;
                if(!states.containsKey(next)) {
                    states.put(next, result.addState(validPairs.contains(Pair.of(target, letter))));
                }
                result.addTransition(states.get(origin), target, states.get(next));
            }
        }
        Queue<Pair<G, G>> workList = new LinkedList<>(validPairs);
        while(!workList.isEmpty()) {
            Pair<G, G> current = workList.poll();
            for(G letter: alphabet) {
                if(!validPairs.contains(Pair.of(current.getSecond(), letter))) continue;
                Pair<G, G> next = Pair.of(current.getFirst(), letter);
                if(!states.containsKey(next)) {
                    states.put(next, result.addState(validPairs.contains(Pair.of(next.getSecond(), next.getFirst()))));
                    workList.add(next);
                }
                result.addTransition(states.get(current), letter, states.get(next));
            }
        }
        return result;
    }
}
