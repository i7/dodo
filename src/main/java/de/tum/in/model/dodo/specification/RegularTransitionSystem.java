package de.tum.in.model.dodo.specification;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.words.Alphabet;
import net.automatalib.words.impl.ArrayAlphabet;
import net.automatalib.words.impl.ListAlphabet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * This class represents one regular transition system. To this end, it maintains the alphabet \Sigma of a regular
 * transition system, and two NFAs: the initial NFA which accepts words from \Sigma^* and the transducer NFA which
 * accepts words from (\Sigma x \Sigma)*.
 * @author Christoph Welzel
 */
public class RegularTransitionSystem {
    public enum Topology { RING, BOW, CROWD, UNKNOWN }
    public Set<String> getSigma() {
        return sigma;
    }

    private final Set<String> sigma;

    public Topology getTopology() {
        return topology;
    }

    private final Topology topology;
    private final int k;
    private final CompactNFA<String> initialer;
    private final CompactNFA<LetterPair<String, String>> transducer;
    private final Map<String, CompactNFA<String>> properties;

    private RegularTransitionSystem(ArrayAlphabet<String> sigma, CompactNFA<String> initialer, CompactNFA<LetterPair<String, String>> transducer, Map<String, CompactNFA<String>> properties, Topology topology, int k) {
        this.sigma = new HashSet<>(sigma);
        this.initialer = initialer;
        this.transducer = transducer;
        this.properties = properties;
        this.topology = topology;
        this.k = k;
    }

    public int getK() {
        if(topology != Topology.CROWD) {
            throw new IllegalStateException("There is no useful value for k in non crowd topologies");
        }
        return k;
    }

    public JSONObject getStats() {
        JSONObject result = new JSONObject();
        result.put("sizeOfInitialLanguage", this.initialer.size());
        result.put("sizeOfTransitionLanguage", this.transducer.size());
        result.put("topology", this.topology);
        result.put("sizeOfAlphabet", this.sigma.size());
        return result;
    }



    public CompactNFA<String> getProperty(String key) {
        return this.properties.getOrDefault(key, null);
    }

    public CompactNFA<String> getInitialer() {
        return this.initialer;
    }

    public CompactNFA<LetterPair<String, String>> getTransducer() {
        return this.transducer;
    }

    static public RegularTransitionSystem parseJSON(String json) {
        Logger parseLogger = Logger.getLogger("rts-parsing");
        parseLogger.setLevel(Level.ALL);
        Handler handler = new ConsoleHandler();
        parseLogger.addHandler(handler);
        class Context {
            private final ArrayAlphabet<String> sigma;

            public Context(JSONObject rts) {
                JSONArray alphabet = rts.getJSONArray("alphabet");
                this.sigma = new ArrayAlphabet<>(alphabet.toList().toArray(new String[alphabet.length()]));
                parseLogger.fine("Extracted alphabet: " + sigma);
            }

            private static <T> BiMap<String, Integer> addStates(CompactNFA<T> nfa, Collection<String> states) {
                BiMap<String, Integer> mapping = HashBiMap.create(states.size());
                for(String state: states) {
                    mapping.put(state, nfa.addState());
                }
                return mapping;
            }

            private CompactNFA<String> parseConfigurationAutomaton(JSONObject nfa) {
                JSONArray transitions = nfa.getJSONArray("transitions");
                CompactNFA<String> automaton = new CompactNFA<>(this.sigma);
                JSONArray states = nfa.getJSONArray("states");
                JSONArray acceptingStates = nfa.getJSONArray("acceptingStates");
                String initialState = nfa.getString("initialState");
                BiMap<String, Integer> stateMapping = Context.addStates(automaton, Arrays.stream(states.toList().toArray(new String[states.length()])).toList());
                automaton.setInitial(stateMapping.get(initialState), true);
                for(int i = 0; i < acceptingStates.length(); i++) {
                    automaton.setAccepting(stateMapping.get(acceptingStates.getString(i)), true);
                }
                for(int i = 0; i < transitions.length(); i++) {
                    JSONObject transition = transitions.getJSONObject(i);
                    Integer origin = stateMapping.get(transition.getString("origin"));
                    Integer target = stateMapping.get(transition.getString("target"));
                    String letterPattern = transition.getString("letter");
                    Pattern pat = Pattern.compile(letterPattern);
                    List<String> letters = this.sigma.stream().filter(pat.asMatchPredicate()).toList();
                    if(letters.isEmpty()) {
                        throw new RuntimeException("Pattern <" + letterPattern + "> did not match any letter of the alphabet");
                    } else {
                        parseLogger.fine("Pattern <" + letterPattern + "> matched to " + letters);
                    }
                    for(String letter: letters) {
                        automaton.addTransition(origin, letter, target);
                    }
                }
                return automaton;
            }

            public CompactNFA<LetterPair<String, String>> parseTransducer(JSONObject nfa) {
                Set<String> sigma = new HashSet<>(this.sigma);
                List<LetterPair<String, String>> alphabetLetters = Sets.cartesianProduct(sigma, sigma).stream()
                        .map((list) -> LetterPair.of(list.get(0), list.get(1)))
                        .toList();
                JSONArray transitions = nfa.getJSONArray("transitions");
                Alphabet<LetterPair<String, String>> alphabet = new ListAlphabet<>(alphabetLetters);
                CompactNFA<LetterPair<String, String>> automaton = new CompactNFA<>(alphabet);
                JSONArray states = nfa.getJSONArray("states");
                JSONArray acceptingStates = nfa.getJSONArray("acceptingStates");
                String initialState = nfa.getString("initialState");
                BiMap<String, Integer> stateMapping = Context.addStates(automaton, Arrays.stream(states.toList().toArray(new String[states.length()])).toList());
                automaton.setInitial(stateMapping.get(initialState), true);
                for(int i = 0; i < acceptingStates.length(); i++) {
                    automaton.setAccepting(stateMapping.get(acceptingStates.getString(i)), true);
                }
                for(int i = 0; i < transitions.length(); i++) {
                    JSONObject transition = transitions.getJSONObject(i);
                    String origin = transition.getString("origin");
                    String target = transition.getString("target");
                    String letterPattern = transition.getString("letter");
                    Pattern pat = Pattern.compile(letterPattern);
                    List<LetterPair<String, String>> letters = alphabetLetters.stream().filter((pair) -> pat.asMatchPredicate().test(pair.from() + "," + pair.to())).toList();
                    if(letters.isEmpty()) {
                        parseLogger.warning("Pattern <" + letterPattern + "> did not match any letter of the alphabet");
                    } else {
                        parseLogger.fine("Pattern <" + letterPattern + "> matched to " + letters);
                    }
                    for (LetterPair<String, String> letter: letters) {
                        automaton.addTransition(stateMapping.get(origin), letter, stateMapping.get(target));
                    }
                }
                return automaton;
            }
        }
        JSONObject rts = new JSONObject(json);
        Context ctx = new Context(rts);
        Map<String, CompactNFA<String>> properties = new HashMap<>();
        JSONObject propertiesObject = rts.getJSONObject("properties");
        for(String key: propertiesObject.keySet()) {
            assert !"deadlock".equals(key);
            properties.put(key, ctx.parseConfigurationAutomaton(propertiesObject.getJSONObject(key)));
        }
        CompactNFA<LetterPair<String, String>> transducer = ctx.parseTransducer(rts.getJSONObject("transducer"));
        CompactNFA<String> deadlock = new CompactNFA<>(ctx.sigma);
        int deadlockThreshold = rts.getInt("deadlockThreshold");
        Map<Pair<Set<Integer>, Integer>, Integer> deadlockStates = new HashMap<>();
        Queue<Pair<Set<Integer>, Integer>> workList = new LinkedList<>();
        deadlockStates.put(Pair.of(transducer.getInitialStates(), 0), deadlock.addInitialState(transducer.getInitialStates().stream().noneMatch(transducer::isAccepting) && deadlockThreshold == 0));
        workList.add(Pair.of(transducer.getInitialStates(), 0));
        while(!workList.isEmpty()) {
            Pair<Set<Integer>, Integer> current = workList.poll();
            for(String currentLetter: ctx.sigma) {
                Set<Integer> next = new HashSet<>();
                for(LetterPair<String, String> letter : transducer.getInputAlphabet()) {
                    if(!currentLetter.equals(letter.from())) {
                        continue;
                    }
                    for(Integer c : current.getFirst()) {
                        next.addAll(transducer.getSuccessors(c, letter));
                    }
                }
                Pair<Set<Integer>, Integer> nextState = Pair.of(next, current.getSecond() == deadlockThreshold ? current.getSecond() : current.getSecond() + 1);
                if(!deadlockStates.containsKey(nextState)) {
                    deadlockStates.put(nextState, deadlock.addState(next.stream().noneMatch(transducer::isAccepting) && deadlockThreshold == nextState.getSecond()));
                    workList.add(nextState);
                }
                deadlock.addTransition(deadlockStates.get(current), currentLetter, deadlockStates.get(nextState));
            }
        }
        properties.put("deadlock", deadlock);
        Topology topology = Topology.UNKNOWN;
        int k = 0;
        if(rts.has("topology")) {
            String name;
            try {
                name = rts.getString("topology");
            } catch(JSONException e) {
                JSONObject t = rts.getJSONObject("topology");
                name = t.getString("topology");
                if(name.equals("crowd")) {
                    k = t.getInt("k");
                }
            }
            switch(name) {
                case "crowd" -> topology = Topology.CROWD;
                case "ring" -> topology = Topology.RING;
                case "bow" -> topology = Topology.BOW;
            }
        }
        return new RegularTransitionSystem(ctx.sigma,
                ctx.parseConfigurationAutomaton(rts.getJSONObject("initial")),
                transducer,
                properties,
                topology,
                k);
    }
}