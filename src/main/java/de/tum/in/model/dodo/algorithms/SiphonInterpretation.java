package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.ts.acceptors.AcceptorTS;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.ListAlphabet;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class SiphonInterpretation extends SetInterpretation  {
    private final boolean cacheOneShot;

    public SiphonInterpretation(RegularTransitionSystem rts) {
        super(rts);
        this.cacheOneShot = true;
    }

    public SiphonInterpretation(RegularTransitionSystem rts, boolean cacheOneShot) {
        super(rts);
        this.cacheOneShot = cacheOneShot;
    }

    @Override
    protected CompactDFA<LetterPair<String, Set<String>>> constructInterpretationAutomaton() {
        LinkedList<LetterPair<String, Set<String>>> letters = new LinkedList<>();
        for (String s : this.rts.getInitialer().getInputAlphabet()) {
            for (Set<String> l : this.interpretationAlphabet) {
                letters.add(LetterPair.of(s, l));
            }
        }
        Alphabet<LetterPair<String, Set<String>>> alphabet = new ListAlphabet<>(letters);
        CompactDFA<LetterPair<String, Set<String>>> result = new CompactDFA<>(alphabet);
        Integer initial = result.addInitialState(true);
        Integer sinkState = result.addState(false);
        for (LetterPair<String, Set<String>> l : alphabet) {
            if (l.to().contains(l.from())) {
                result.addTransition(initial, l, sinkState);
            } else {
                result.addTransition(initial, l, initial);
            }
            result.addTransition(sinkState, l, sinkState);
        }
        return result;
    }

    @Override
    public OneShot oneShot(CompactNFA<String> badWords) {
        record State(Integer initial, List<Integer> ost, Integer bad) {}
        EmptinessGraph<State, LetterPair<String, String>> emptiness = new EmptinessGraph<>() {
            record GameState(List<Integer> marked, List<Integer> open, String remove, Set<String> removed, List<Integer> target) {}
            private final HashMap<GameState, Set<List<Integer>>> gameWinningResultsCache = new HashMap<>();

            private void updateResult(boolean isProgress, Integer origin, List<Integer> newMarked, List<Integer> newOpen, GameState gameState, Set<List<Integer>> result) {
                for(LetterPair<String, String> letter: rts.getTransducer().getLocalInputs(origin)) {
                    // check that the origin is part of the removed set
                    if(!gameState.removed().contains(letter.from())) continue;
                    for(Integer target: rts.getTransducer().getSuccessors(origin, letter)) {
                        // check for progress
                        if(!isProgress && gameState.removed.contains(letter.to()) && gameState.target.contains(target)) continue;
                        Set<String> newRemoved = new HashSet<>(gameState.removed);
                        newRemoved.add(letter.to());
                        List<Integer> newTarget = new LinkedList<>(gameState.target);
                        if(!newTarget.contains(target)) {
                            newTarget.add(target);
                        }
                        result.addAll(gameWinningResults(new GameState(
                                newMarked,
                                newOpen,
                                gameState.remove,
                                newRemoved,
                                newTarget
                        )));
                    }
                }
            }
            private Set<List<Integer>> gameWinningResults(GameState gameState) {
                if(cacheOneShot && gameWinningResultsCache.containsKey(gameState)) return gameWinningResultsCache.get(gameState);
                Set<List<Integer>> result = new HashSet<>();
                // these are the cases where we don't make progress in the origin
                for(Integer origin: gameState.marked()) {
                    updateResult(false, origin, gameState.marked(), gameState.open(), gameState, result);
                }
                // in contrast, these are the cases where we make progress in the origin
                if(!gameState.open().isEmpty()) {
                    Integer progressedState = gameState.open().get(0);
                    List<Integer> newMarked = new LinkedList<>(gameState.marked());
                    newMarked.add(progressedState);
                    List<Integer> newOpen = new LinkedList<>(gameState.open());
                    newOpen.remove(0);
                    updateResult(true, progressedState, newMarked, newOpen, gameState, result);
                } else {
                    if(gameState.removed().contains(gameState.remove())) {
                        result.add(gameState.target);
                    }
                }
                // it remains to populate the cache
                if(cacheOneShot) {
                    gameWinningResultsCache.put(gameState, result);
                }
                return result;
            }

            @Override
            protected Set<Pair<LetterPair<String, String>, State>> getTransitions(State state) {
                Set<Pair<LetterPair<String, String>, State>> result = new HashSet<>();
                for(String origin: rts.getInitialer().getLocalInputs(state.initial())) {
                    for(String target: badWords.getLocalInputs(state.bad())) {
                        for(Integer nextInitial: rts.getInitialer().getSuccessors(state.initial(), origin)) {
                            for(Integer nextBad: badWords.getSuccessors(state.bad(), target)) {
                                Set<List<Integer>> cache = gameWinningResults(new GameState(List.of(), state.ost(), target, Set.of(origin), List.of()));
                                for(List<Integer> nextOST: cache) {
                                    result.add(Pair.of(
                                            LetterPair.of(origin, target),
                                            new State(nextInitial, nextOST, nextBad)
                                    ));
                                }
                            }
                        }
                    }
                }
                return result;
            }

            @Override
            protected boolean isFinal(State state) {
                return (rts.getInitialer().isAccepting(state.initial())
                        && state.ost.stream().allMatch(rts.getTransducer()::isAccepting)
                        && badWords.isAccepting(state.bad()));
            }

            private static Set<List<Integer>> allUniqueWords(Set<Integer> elements) {
                return allUniqueWords(elements, Set.of(List.of()));
            }
            private static Set<List<Integer>> allUniqueWords(Set<Integer> elements, Set<List<Integer>> current) {
                if(elements.isEmpty()) {
                    return current;
                }
                Set<List<Integer>> result = new HashSet<>();
                for(Integer element: elements) {
                    Set<List<Integer>> next = new HashSet<>(current);
                    next.addAll(current.stream().map((l) -> {List<Integer> r = new LinkedList<>(l); r.add(element); return r;}).collect(Collectors.toSet()));
                    result.addAll(allUniqueWords(elements.stream().filter(Predicate.not(element::equals)).collect(Collectors.toSet()), next));
                }
                return result;
            }


            @Override
            protected Set<State> getInitial() {
                Set<State> result = new HashSet<>();
                for(Integer initial: rts.getInitialer().getInitialStates()) {
                    for(Integer bad: badWords.getInitialStates()) {
                        for(List<Integer> ost: allUniqueWords(rts.getTransducer().getInitialStates())) {
                            result.add(new State(initial, ost, bad));
                        }
                    }
                }
                return result;
            }
        };
        return new OneShot.IntegerProxy<>(emptiness);
    }

    @Override
    public boolean accepts(RegularTransitionSystem rts, Word<String> word, Word<Set<String>> statement) {
        return word.length() == statement.length() && IntStream.range(0, word.length()).mapToObj((x) -> !statement.getSymbol(x).contains(word.getSymbol(x))).allMatch((x) -> x);
    }

    private boolean refineSeparator(CompactNFA<LetterPair<String, String>> transducer, List<Set<String>> separator) {
        int n = separator.size();
        record Node(int depth, Integer state) {}
        Set<Node> yellowNodes = new HashSet<>();
        Set<Node> redNodes = new HashSet<>();
        Map<Node, LetterPair<String, String>> words = new HashMap<>();
        Map<Node, Node> backtrack = new HashMap<>();
        Stack<Node> workStack = new Stack<>();
        for (Integer initial : transducer.getInitialStates()) {
            Node initialNode = new Node(0, initial);
            yellowNodes.add(initialNode);
            workStack.push(initialNode);
        }
        while(!workStack.isEmpty()) {
            Node current = workStack.pop();
            if(current.depth() == n && redNodes.contains(current) && transducer.isAccepting(current.state())) {
                for(int i = n-1; 0 <= i; i--) {
                    LetterPair<String, String> letter = words.get(current);
                    separator.get(i).remove(letter.to());
                    current = backtrack.get(current);
                }
                return true;
            }
            if(current.depth() == n) continue;
            for(LetterPair<String, String> input: transducer.getLocalInputs(current.state())) {
                if(separator.get(current.depth()).contains(input.from())) continue;
                for(Integer nextState: transducer.getSuccessors(current.state(), input)) {
                    Node next = new Node(current.depth()+1, nextState);
                    if(redNodes.contains(next)) continue;
                    if(redNodes.contains(current) || separator.get(current.depth()).contains(input.to())) {
                        backtrack.put(next, current);
                        words.put(next, input);
                        redNodes.add(next);
                        workStack.add(next);
                    } else {
                        if(yellowNodes.contains(next)) continue;
                        backtrack.put(next, current);
                        words.put(next, input);
                        yellowNodes.add(next);
                        workStack.add(next);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Word<Set<String>> disprove(RegularTransitionSystem rts, Word<String> source, Word<String> target) {
        assert source.length() == target.length();
        LinkedList<Set<String>> separator = new LinkedList<>(source.stream().map((t) -> this.allLetters.stream().filter((x) -> !t.equals(x)).collect(Collectors.toSet())).toList());
        while(!this.accepts(rts, target, Word.fromList(separator))) {
            if(!refineSeparator(rts.getTransducer(), separator)) return Word.fromList(separator);
        }
        return null;
    }

    @Override
    public boolean isNonInductive(RegularTransitionSystem rts, Word<Set<String>> statement) {
        record State(Integer transducer, int length, boolean intersected) {}
        EmptinessGraph<State, LetterPair<String, String>> findViolation = new EmptinessGraph<>() {
            @Override
            protected boolean isFinal(State state) {
                return rts.getTransducer().isAccepting(state.transducer) && state.length == statement.length() && state.intersected;
            }

            @Override
            protected Set<Pair<LetterPair<String, String>, State>> getTransitions(State state) {
                Set<Pair<LetterPair<String, String>, State>> result = new HashSet<>();
                if(state.length == statement.length()) {
                    return result;
                }
                Set<String> statementLetter = statement.getSymbol(state.length);
                for(LetterPair<String, String> input: rts.getTransducer().getLocalInputs(state.transducer)) {
                    if(statementLetter.contains(input.from())) {
                        continue;
                    }
                    for(Integer nextTransducer: rts.getTransducer().getSuccessors(state.transducer, input)) {
                        result.add(Pair.of(input, new State(nextTransducer, state.length+1, state.intersected || statementLetter.contains(input.to()))));
                    }
                }
                return result;
            }

            @Override
            protected Set<State> getInitial() {
                Set<State> result = new HashSet<>();
                for(Integer initial: rts.getTransducer().getInitialStates()) {
                    result.add(new State(initial, 0, false));
                }
                return result;
            }
        };
        return findViolation.findAcceptedWord().isPresent();
    }

    @Override
    public <S> Word<Set<String>> findInductiveCounterExample(RegularTransitionSystem rts, DFA<S, Set<String>> hypothesis, Collection<? extends Set<String>> hypothesisAlphabet) {
        record  State<S>(Integer transducer, boolean intersected, S hypothesisState) {}
        EmptinessGraph<State<S>, Set<String>> findViolation = new EmptinessGraph<>() {
            @Override
            protected boolean isFinal(State<S> state) {
                return rts.getTransducer().isAccepting(state.transducer) && state.intersected && hypothesis.isAccepting(state.hypothesisState);
            }

            @Override
            protected Set<Pair<Set<String>, State<S>>> getTransitions(State state) {
                Set<Pair<Set<String>, State<S>>> result = new HashSet<>();
                for(LetterPair<String, String> transducerLetter: rts.getTransducer().getLocalInputs(state.transducer)) {
                    for (Set<String> letter : hypothesisAlphabet) {
                        if(letter.contains(transducerLetter.from())) {
                            continue;
                        }
                        S nextHypothesis = hypothesis.getSuccessor((S)state.hypothesisState, letter);
                        if(nextHypothesis == null) {
                            continue;
                        }
                        for(Integer nextTransducer: rts.getTransducer().getSuccessors(state.transducer, transducerLetter)) {
                            result.add(Pair.of(letter, new State<>(nextTransducer, state.intersected || letter.contains(transducerLetter.to()), nextHypothesis)));
                        }
                    }
                }
                return result;
            }

            @Override
            protected Set<State<S>> getInitial() {
                Set<State<S>> result = new HashSet<>();
                for(Integer initialTransducer: rts.getTransducer().getInitialStates()) {
                    for(S initialHypothesis: hypothesis.getInitialStates()) {
                        result.add(new State<>(initialTransducer, false, initialHypothesis));
                    }
                }
                return result;
            }
        };
        return findViolation.findAcceptedWord().orElse(null);
    }

    @Override
    public <S> CompactNFA<LetterPair<String, String>> nonAbstractlyReachable(RegularTransitionSystem rts, AcceptorTS<S, Set<String>> inductiveStatements, Collection<? extends Set<String>> inputs) {
        record State<S>(boolean intersectedBefore, S statements, boolean intersectedAfter) {}
        CompactNFA<LetterPair<String, String>> result = new CompactNFA<>(rts.getTransducer().getInputAlphabet());
        Map<State<S>, Integer> states = new HashMap<>();
        Queue<State<S>> workList = new LinkedList<>();
        for(S statements: inductiveStatements.getInitialStates()) {
            State<S> state = new State<>(false, statements, false);
            workList.add(state);
            states.put(state, result.addInitialState(false));
        }
        while(!workList.isEmpty()) {
            State<S> state = workList.poll();
            for(LetterPair<String, String> letter: result.getInputAlphabet()) {
                for(Set<String> statementLetter: inputs) {
                    for(S nextStatements: inductiveStatements.getSuccessors(state.statements, statementLetter)) {
                        State<S> next = new State<>(
                                state.intersectedBefore || statementLetter.contains(letter.from()),
                                nextStatements,
                                state.intersectedAfter || statementLetter.contains(letter.to()));
                        if(!states.containsKey(next)) {
                            workList.add(next);
                            states.put(next, result.addState(
                                    !next.intersectedBefore
                                            && inductiveStatements.isAccepting(next.statements)
                                            && next.intersectedAfter));
                        }
                        result.addTransition(
                                states.get(state),
                                letter,
                                states.get(next));
                    }
                }
            }
        }
        return result;
    }
}
