package de.tum.in.model.dodo.algorithms;

import de.learnlib.algorithms.lstar.dfa.ClassicLStarDFA;
import de.learnlib.algorithms.lstar.dfa.ExtensibleLStarDFA;
import de.learnlib.api.query.DefaultQuery;
import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.util.automata.fsa.DFAs;
import net.automatalib.util.automata.fsa.NFAs;
import net.automatalib.words.Word;
import net.automatalib.words.impl.GrowingMapAlphabet;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class Learning {
    private final RegularTransitionSystem rts;
    private final List<TeacherLearner<?>> learners;
    private final boolean finalCheck;
    private CompactDFA<LetterPair<String, String>> abstraction;
    private final CompactNFA<String> bad;

    public Learning(RegularTransitionSystem rts, List<InterpretationAbstractorPair<?>> interpretations, CompactNFA<String> bad, boolean finalCheck) {
        this.finalCheck = finalCheck;
        this.rts = rts;
        this.learners = new LinkedList<>();
        for(InterpretationAbstractorPair interpretation: interpretations) {
            InterpretationTeacher<?> teacher = new InterpretationTeacher<>(interpretation.interpretation, rts, bad);
            learners.add(new TeacherLearner(teacher, interpretation.abstractor));
        }
        this.abstraction = Util.combine(this.rts.getInitialer(), bad);
        this.bad = bad;
    }

    public JSONObject getStats() {
        JSONObject result = new JSONObject();
        result.put("finalAbstraction", DFAs.minimize(this.getAggregatedAbstraction()).size());
        result.put("finalPotentiallyReach", DFAs.minimize(this.getAggregatedReach()).size());
        JSONArray learners = new JSONArray();
        for(TeacherLearner<?> l: this.learners) {
            learners.put(l.getStats());
        }
        result.put("learners", learners);
        return result;
    }

    private CompactDFA<LetterPair<String, String>> getAggregatedAbstraction() {
        CompactDFA<LetterPair<String, String>> result = null;
        for(TeacherLearner<?> learner: this.learners) {
            if(result == null) {
                result = learner.getLearnedAbstraction();
            } else {
                result = DFAs.and(result, learner.getLearnedAbstraction(), this.rts.getTransducer().getInputAlphabet());
            }
        }
        return result;
    }

    private CompactDFA<String> getAggregatedReach() {
        CompactDFA<LetterPair<String, String>> potentiallyReach = this.getAggregatedAbstraction();
        CompactDFA<String> result = NFAs.determinize(TransducerStep.stepAutomaton(this.rts.getInitialer(), potentiallyReach));
        return result;
    }


    public record InterpretationAbstractorPair<G>(Interpretation<G> interpretation, Abstractor<G> abstractor) {
        public static <G> InterpretationAbstractorPair<G> createNoOp(RegularTransitionSystem rts, Interpretation<G> interpretation) {
            return new InterpretationAbstractorPair<>(interpretation, new NoOpAbstractor<>(rts, interpretation));
        }

    }


    private void start() {
        for(TeacherLearner<?> current: this.learners) {
            current.start();
            current.makeInductive();
            CompactDFA<LetterPair<String, String>> refinement = current.getLearnedAbstraction();
            this.abstraction = DFAs.and(this.abstraction, refinement, refinement.getInputAlphabet());
        }
    }

    private boolean step() throws Unlearnable {
        Word<LetterPair<String, String>> ce = Util.getAcceptedWord(this.abstraction);
        if(ce == null) {
            return false;
        }
        for(TeacherLearner<?> current: this.learners) {
            CompactDFA<LetterPair<String, String>> refinement = current.remove(ce);
            if(refinement != null) {
                this.abstraction = DFAs.and(this.abstraction, refinement, this.rts.getTransducer().getInputAlphabet());
                return true;
            }
        }
        throw new Unlearnable(ce, this.getStats());
    }

    public List<TeacherLearner<?>> learn() throws Unlearnable {
        start();
        while(step()) { }
        if(this.finalCheck) {
            Word<LetterPair<String, String>> word = Util.getAcceptedWord(
                    DFAs.and(
                            this.getAggregatedAbstraction(),
                            Util.combine(this.rts.getInitialer(), this.bad),
                            this.rts.getTransducer().getInputAlphabet()));
            if(word != null) {
                throw new IllegalStateException("Failed final check: " + word.toString());
            }

        }
        return this.learners;
    }

    public static class TeacherLearner<G> {
        private final InterpretationTeacher<G> teacher;
        private final ExtensibleLStarDFA<G> learner;
        public ExtensibleLStarDFA<G> getLearner() {
            return learner;
        }

        private final Abstractor abstractor;
        private final Set<G> inputs;

        public Set<G> getInputs() {
            return inputs;
        }

        private int removingNonInductiveStatements = 0;
        private int disprovingCounterExample = 0;
        private int artifactStatement = 0;
        private int generalizedStatement = 0;

        public TeacherLearner(InterpretationTeacher<G> teacher, Abstractor abstractor) {
            this.teacher = teacher;
            this.inputs = new HashSet<>();
            this.learner = new ClassicLStarDFA<>(new GrowingMapAlphabet<>(), teacher);
            this.abstractor = abstractor;
        }

        public JSONObject getStats() {
            JSONObject result = new JSONObject();
            result.put("learnerName", this.learner.getClass().getName());
            result.put("name", this.teacher.getInterpretation().getClass().getName());
            result.put("abstractor", this.abstractor.getClass().getName());
            result.put("removingNonInductive", this.removingNonInductiveStatements);
            result.put("disprovingCounterexamples", this.disprovingCounterExample);
            result.put("artifactStatement", this.artifactStatement);
            result.put("generalizedStatement", this.generalizedStatement);
            result.put("sumOfGeneralizedLanguages", this.abstractor.accumulatedSize());
            result.put("learnedLanguageSize", this.learner.getHypothesisModel().size());
            result.put("learnedAbstractionSize", this.getLearnedAbstraction().size());
            result.put("usedAlphabetSymbolsSize", this.inputs.size());
            result.put("usedAlphabetSymbols", new JSONArray(this.inputs));
            return result;
        }

        public void start() {
            this.learner.startLearning();
        }

        public CompactDFA<LetterPair<String, String>> remove(Word<LetterPair<String, String>> ce) {
            disprovingCounterExample++;
            Word<G> statement = teacher.getInterpretation().disprove(teacher.getRts(), ce);
            if(statement != null) {
                for(G letter: statement) {
                    if(!this.inputs.contains(letter)) {
                        this.inputs.add(letter);
                        this.learner.addAlphabetSymbol(letter);
                    }
                }
                if(abstractor.generalize(statement)) {
                    generalizedStatement++;
                    return abstractor.getAbstraction();
                } else {
                    artifactStatement++;
                    if(!this.learner.getHypothesisModel().accepts(statement)) {
                        this.learner.refineHypothesis(new DefaultQuery<>(statement));
                    }
                    makeInductive();
                    return getLearnedAbstraction();
                }
            }
            return null;
        }

        public void makeInductive() {
            Word<G> ce;
            while(true) {
                ce = teacher.getInterpretation().findInductiveCounterExample(this.teacher.getRts(), learner.getHypothesisModel(), this.inputs);
                if(ce != null) {
                    removingNonInductiveStatements++;
                    learner.refineHypothesis(new DefaultQuery<>(ce));
                } else {
                    break;
                }
            }
        }

        public CompactDFA<LetterPair<String, String>> getLearnedAbstraction() {
            CompactNFA<LetterPair<String, String>> nonAbstraction = this.teacher.getInterpretation().nonAbstractlyReachable(teacher.getRts(), this.learner.getHypothesisModel(), this.inputs);
            return DFAs.complement(NFAs.determinize(nonAbstraction), nonAbstraction.getInputAlphabet());
        }

    }

    static public class Unlearnable extends Exception {
        final public Word<LetterPair<String, String>> unremovable;
        final public JSONObject stats;
        public Unlearnable(Word<LetterPair<String, String>> unremovable, JSONObject stats) {
            this.unremovable = unremovable;
            this.stats = stats;
        }
    }

    static public List<TeacherLearner<?>> learn(RegularTransitionSystem rts, List<Interpretation<?>> interpretations, CompactNFA<String> bad) throws Unlearnable {
        List<InterpretationAbstractorPair<?>> l = new LinkedList<>();
        for(Interpretation<?> interpretation: interpretations) {
            l.add(InterpretationAbstractorPair.createNoOp(rts, interpretation));
        }
        Learning learning = new Learning(rts, l, bad, false);
        return learning.learn();
    }

}
