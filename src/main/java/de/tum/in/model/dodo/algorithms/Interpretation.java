
package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.DFA;
import net.automatalib.automata.fsa.impl.compact.CompactDFA;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.commons.util.Pair;
import net.automatalib.ts.acceptors.AcceptorTS;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.TimeoutException;


import java.util.*;
import java.util.stream.IntStream;

/*
An abstract representation of any interpretation. The type parameter G captures the alphabet of the interpretation.
*/
public abstract class Interpretation<G> {
    abstract public CompactDFA<LetterPair<String, G>> getInterpretationAutomaton(RegularTransitionSystem rts);

    abstract public Alphabet<G> getInterpretationAlphabet(RegularTransitionSystem rts);

    abstract public OneShot oneShot(CompactNFA<String> badWords);

    public boolean accepts(RegularTransitionSystem rts, Word<String> word, Word<G> statement) {
        assert word.length() == statement.length();
        CompactDFA<LetterPair<String, G>> interpretation = this.getInterpretationAutomaton(rts);
        return interpretation.accepts(IntStream.range(0, word.length()).mapToObj((x) -> LetterPair.of(word.getSymbol(x), statement.getSymbol(x))).toList());
    }

    public CompactNFA<G> getNonInductive(RegularTransitionSystem rts) {
        record State(Integer interpretationBefore, Integer transducer, Integer interpretationAfter) {}
        CompactNFA<G> result = new CompactNFA<>(this.getInterpretationAlphabet(rts));
        CompactDFA<LetterPair<String, G>> interpretationAutomaton = this.getInterpretationAutomaton(rts);
        CompactNFA<LetterPair<String, String>> transducer = rts.getTransducer();
        Map<State, Integer> stateMapping = new HashMap<>();
        Queue<State> workQueue = new LinkedList<>(transducer.getInitialStates().stream().map(
                (initialState) -> {
                    State p = new State(interpretationAutomaton.getInitialState(),  initialState, interpretationAutomaton.getInitialState());
                    stateMapping.put(p, result.addInitialState(false));
                    return p;
                }).toList());
        while(!workQueue.isEmpty()) {
            State current = workQueue.poll();
            Integer currentState = stateMapping.get(current);
            for(G statementLetter: this.getInterpretationAlphabet(rts)) {
                for(LetterPair<String, String> transducerLetter: transducer.getLocalInputs(current.transducer)) {
                    LetterPair<String, G> before = LetterPair.of(transducerLetter.from(), statementLetter);
                    LetterPair<String, G> after = LetterPair.of(transducerLetter.to(), statementLetter);
                    for(Integer nextTransducer: transducer.getSuccessors(current.transducer, transducerLetter)) {
                        State next = new State(interpretationAutomaton.getSuccessor(current.interpretationBefore, before), nextTransducer, interpretationAutomaton.getSuccessor(current.interpretationAfter, after));
                        if(!stateMapping.containsKey(next)) {
                            stateMapping.put(next, result.addState(
                                    interpretationAutomaton.isAccepting(next.interpretationBefore)
                                     && transducer.isAccepting(next.transducer)
                                     && !interpretationAutomaton.isAccepting(next.interpretationAfter)
                            ));
                            workQueue.add(next);
                        }
                        result.addTransition(currentState, statementLetter, stateMapping.get(next));
                    }
                }
            }
        }
        return result;
    }

    public <S> CompactNFA<LetterPair<String, String>> nonAbstractlyReachable(RegularTransitionSystem rts, AcceptorTS<S, G> inductiveStatements, Collection<? extends G> inputs) {
        CompactDFA<LetterPair<String, G>> interpretationAutomaton = this.getInterpretationAutomaton(rts);
        record State<S>(Integer interpretationBefore, S statements, Integer interpretationAfter) {}
        CompactNFA<LetterPair<String, String>> result = new CompactNFA<>(rts.getTransducer().getInputAlphabet());
        Map<State<S>, Integer> states = new HashMap<>();
        Queue<State<S>> workList = new LinkedList<>();
        for(S statements: inductiveStatements.getInitialStates()) {
            State<S> state = new State<>(interpretationAutomaton.getInitialState(), statements, interpretationAutomaton.getInitialState());
            workList.add(state);
            states.put(state, result.addInitialState(false));
        }
        while(!workList.isEmpty()) {
            State<S> state = workList.poll();
            for(LetterPair<String, String> letter: result.getInputAlphabet()) {
                for(G statementLetter: inputs) {
                    for(S nextStatements: inductiveStatements.getSuccessors(state.statements, statementLetter)) {
                        State<S> next = new State<>(
                                interpretationAutomaton.getSuccessor(state.interpretationBefore, LetterPair.of(letter.from(), statementLetter)),
                                nextStatements,
                                interpretationAutomaton.getSuccessor(state.interpretationAfter, LetterPair.of(letter.to(), statementLetter)));
                        if(!states.containsKey(next)) {
                            workList.add(next);
                            states.put(next, result.addState(
                                    interpretationAutomaton.isAccepting(next.interpretationBefore)
                                            && inductiveStatements.isAccepting(next.statements)
                                            && !interpretationAutomaton.isAccepting(next.interpretationAfter)));
                        }
                        result.addTransition(
                                states.get(state),
                                letter,
                                states.get(next));
                    }
                }
            }
        }
        return result;
    }
    
    public Word<G> disprove(RegularTransitionSystem rts, Word<LetterPair<String, String>> words) {
        WordBuilder<String> origin = new WordBuilder<>();
        WordBuilder<String> target = new WordBuilder<>();
        for(LetterPair<String, String> current: words) {
            origin.append(current.from());
            target.append(current.to());
        }
        return this.disprove(rts, origin.toWord(), target.toWord());
    }

    public Word<G> disprove(RegularTransitionSystem rts, Word<String> origin, Word<String> target) {
        assert origin.length() == target.length();
        if(origin.equals(target)) return null;
        CompactDFA<LetterPair<String, G>> interpretationAutomaton = this.getInterpretationAutomaton(rts);
        CompactNFA<G> nonInductiveAutomaton = this.getNonInductive(rts);
        class Indices {
            final private int gammaSize;
            final private Map<G, Integer> gamma;
            final private int n;
            final private int interpretation;
            final private int nonInductive;

            final private ISolver solver;

            public Indices() {
                this.gamma = new HashMap<>();
                for(G letter: getInterpretationAlphabet(rts)) {
                    this.gamma.put(letter, this.gamma.keySet().size());
                }
                this.gammaSize = getInterpretationAlphabet(rts).size();
                this.n = origin.length();
                this.interpretation = interpretationAutomaton.size();
                this.nonInductive = nonInductiveAutomaton.size();
                this.solver = SolverFactory.newDefault();
            }

            public int getLetter(G letter, int position) {
                assert 0 < position;
                assert position <= this.n;
                assert this.gamma.containsKey(letter);
                return ((position-1) * this.gammaSize) + this.gamma.get(letter) + 1;
            }

            public int interpretationBefore(Integer state, int position) {
                assert 0 <= position;
                assert position <= this.n;
                assert 0 <= state;
                assert state < this.interpretation;
                return (this.n * this.gammaSize) + (position * this.interpretation) + state + 1;
            }

            public int interpretationAfter(Integer state, int position) {
                assert 0 <= position;
                assert position <= this.n;
                assert 0 <= state;
                assert state < interpretationAutomaton.size();
                return (this.n * this.gammaSize) + ((this.n + 1) * this.interpretation) + (position * this.interpretation) + state + 1;
            }

            public int nonInductive(Integer state, int position) {
                assert 0 <= position;
                assert position <= this.n;
                assert 0 <= state;
                assert state < nonInductiveAutomaton.size();
                return (this.n * this.gammaSize) + (2 * (this.n + 1) * this.interpretation) + (position * this.nonInductive) + state + 1;
            }

            public void addInitialStates() {
                // mark initial states of all automata
                try {
                    solver.addClause(new VecInt(new int[]{this.interpretationBefore(interpretationAutomaton.getInitialState(), 0)}));
                    solver.addClause(new VecInt(new int[]{this.interpretationAfter(interpretationAutomaton.getInitialState(), 0)}));
                    for(Integer state: nonInductiveAutomaton.getInitialStates()) solver.addClause(new VecInt(new int[]{this.nonInductive(state, 0)}));
                } catch(ContradictionException e) {
                    throw new RuntimeException("Error in propositional encoding: initial states of automata");
                }
            }

            private void restrictGuessedWord(int i) {
                if(i != 0) {
                    // make sure the guess is exactly one word
                    IVecInt any = new VecInt();
                    for (G firstLetter : getInterpretationAlphabet(rts)) {
                        any.push(this.getLetter(firstLetter, i));
                    }
                    try {
                        solver.addExactly(any, 1);
                    } catch (ContradictionException e) {
                        throw new RuntimeException("Error in propositional encoding: exactly one letter at position " + i);
                    }
                }
            }

            private void interpretationRuns(int i) {
                // capture the runs of the interpretation on the word and origin and target, respectively
                IVecInt exactlyOneBefore = new VecInt();
                IVecInt exactlyOneAfter = new VecInt();
                for(Integer interpretationState: interpretationAutomaton.getStates()) {
                    exactlyOneBefore.push(this.interpretationBefore(interpretationState, i));
                    exactlyOneAfter.push(this.interpretationAfter(interpretationState, i));
                    if(i < n) {
                        for (LetterPair<String, G> letter : interpretationAutomaton.getLocalInputs(interpretationState)) {
                            if(letter.from().equals(origin.getSymbol(i))) {
                                IVecInt nextStateImplication = new VecInt();
                                nextStateImplication.push(-this.interpretationBefore(interpretationState, i));
                                nextStateImplication.push(-this.getLetter(letter.to(), i + 1));
                                nextStateImplication.push(this.interpretationBefore(interpretationAutomaton.getSuccessor(interpretationState, letter), i+1));
                                try {
                                    solver.addClause(nextStateImplication);
                                } catch(ContradictionException e) {
                                    throw new RuntimeException("Error in propositional encoding: next state of interpretation before at position " + i);
                                }
                            }
                            if(letter.from().equals(target.getSymbol(i))) {
                                IVecInt nextStateImplication = new VecInt();
                                nextStateImplication.push(-this.interpretationAfter(interpretationState, i));
                                nextStateImplication.push(-this.getLetter(letter.to(), i + 1));
                                nextStateImplication.push(this.interpretationAfter(interpretationAutomaton.getSuccessor(interpretationState, letter), i+1));
                                try {
                                    solver.addClause(nextStateImplication);
                                } catch(ContradictionException e) {
                                    throw new RuntimeException("Error in propositional encoding: next state of interpretation after at position " + i);
                                }
                            }
                        }
                    }
                }
                try {
                    solver.addExactly(exactlyOneBefore, 1);
                    solver.addExactly(exactlyOneAfter, 1);
                } catch(ContradictionException e) {
                    throw new RuntimeException("Error in propositional encoding: exactly one interpretation state at position " + i);
                }
            }

            private void nonInductiveRuns(int i) {
                if(i < n) {
                    for(Integer state : nonInductiveAutomaton.getStates()) {
                        for(G letter : nonInductiveAutomaton.getLocalInputs(state)) {
                            for(Integer next : nonInductiveAutomaton.getSuccessors(state, letter)) {
                                IVecInt nextStateImplication = new VecInt();
                                nextStateImplication.push(-this.nonInductive(state, i));
                                nextStateImplication.push(-this.getLetter(letter, i+1));
                                nextStateImplication.push(this.nonInductive(next, i+1));
                                try {
                                    solver.addClause(nextStateImplication);
                                } catch(ContradictionException e) {
                                    throw new RuntimeException("Error in propositional encoding: transition implication for nonInductiveAutomaton");
                                }
                            }
                        }
                    }
                }
            }

            public void allWordPositions() {
                // iterate over all positions
                for(int i = 0; i <= n; i++) {
                    this.restrictGuessedWord(i);
                    this.interpretationRuns(i);
                    this.nonInductiveRuns(i);
                }
            }

            public void checkAcceptance() {
                try {
                    IVecInt oneStateBefore = new VecInt();
                    for (Integer f : interpretationAutomaton.getStates()) {
                        if (!interpretationAutomaton.isAccepting(f)) continue;
                        oneStateBefore.push(this.interpretationBefore(f, n));
                        solver.addClause(new VecInt(new int[]{-this.interpretationAfter(f, n)}));
                    }
                    solver.addClause(oneStateBefore);
                    for(Integer f: nonInductiveAutomaton.getStates()) {
                        if(!nonInductiveAutomaton.isAccepting(f)) continue;
                        solver.addClause(new VecInt(new int[]{-this.nonInductive(f, n)}));
                    }
                } catch(ContradictionException e) {
                    throw new RuntimeException("Error in propositional encoding: final states of the automata");
                }
            }

            public Word<G> extractSolution() {
                try {
                    if (solver.isSatisfiable()) {
                        WordBuilder<G> builder = new WordBuilder<>(n);
                        withNextSymbol: for(int i = 1; i <= n; i++) {
                            for(G letter: getInterpretationAlphabet(rts)) {
                                if(solver.model(this.getLetter(letter, i))) {
                                    builder.append(letter);
                                    continue withNextSymbol;
                                }
                            }
                            throw new RuntimeException("Error in propositional encoding: no letter chosen for position " + i);
                        }
                        return builder.toWord();
                    } else {
                        return null;
                    }
                } catch(TimeoutException e) {
                    return null;
                }
            }
        }
        Indices indices = new Indices();
        indices.addInitialStates();
        indices.allWordPositions();
        indices.checkAcceptance();
        return indices.extractSolution();
    }

    public <S> Word<G> findInductiveCounterExample(RegularTransitionSystem rts, DFA<S, G> hypothesis, Collection<? extends G> hypothesisAlphabet) {
        CompactNFA<G> nonInductive = this.getNonInductive(rts);
        record StateTuple<S>(S hypothesisState, Integer nonInductiveState) {}
        EmptinessGraph<StateTuple, G> inductiveIntersection = new EmptinessGraph<>() {
            @Override
            protected boolean isFinal(StateTuple state) {
                S hypothesisState = (S) state.hypothesisState();
                return hypothesis.isAccepting(hypothesisState) && nonInductive.isAccepting(state.nonInductiveState());
            }

            @Override
            protected Set<Pair<G, StateTuple>> getTransitions(StateTuple state) {
                Set<Pair<G, StateTuple>> result = new HashSet<>();
                S hypothesisState = (S) state.hypothesisState();
                for(G input: nonInductive.getLocalInputs(state.nonInductiveState())) {
                    if(!hypothesisAlphabet.contains(input)) {
                        continue;
                    }
                    for(Integer nonInductiveNext: nonInductive.getSuccessors(state.nonInductiveState(), input)) {
                        for(S hypothesisNext: hypothesis.getSuccessors(hypothesisState, input)) {
                            result.add(Pair.of(input, new StateTuple<>(hypothesisNext, nonInductiveNext)));
                        }
                    }
                }
                return result;
            }

            @Override
            protected Set<StateTuple> getInitial() {
                Set<StateTuple> result = new HashSet<>();
                for(S hypothesisInitial: hypothesis.getInitialStates()) {
                    for(Integer nonInductiveInitial: nonInductive.getInitialStates()) {
                        result.add(new StateTuple<>(hypothesisInitial, nonInductiveInitial));
                    }
                }
                return result;
            }
        };
        Optional<Word<G>> result = inductiveIntersection.findAcceptedWord();
        return result.orElse(null);
    }

    public boolean isNonInductive(RegularTransitionSystem rts, Word<G> input) {
        return this.getNonInductive(rts).accepts(input);
    }
}