package de.tum.in.model.dodo;

import com.google.common.collect.Sets;
import de.tum.in.model.dodo.algorithms.*;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.words.Word;
import de.tum.in.model.dodo.specification.LetterPair;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * The main class of this application.
 * @author Christoph Welzel (welzel@in.tum.de)
 */
public class Main {
    static class UnknownProperty extends Exception {
        public final String key;
        public UnknownProperty(String key) {
            this.key = key;
        }
    }

    private static void oneshot(
            String whichInterpretations,
            String filename,
            String property,
            String programCall,
            boolean noCache
    ) throws IOException, UnknownProperty {
        long time = System.currentTimeMillis();
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of(filename)));
        List<SetInterpretation> interpretations = new LinkedList<>();
        for(char i: whichInterpretations.toCharArray()) {
            switch(i) {
                case 'T': System.err.println("Warning: t and T are synonymous for oneshot analysis");
                case 't': interpretations.add(new TrapInterpretation(rts, !noCache)); break;
                case 'S': System.err.println("Warning: s and S are synonymous for oneshot analysis");
                case 's': interpretations.add(new SiphonInterpretation(rts, !noCache)); break;
                case 'f': interpretations.add(new FlowInterpretation(rts)); break;
            }
        }
        CompactNFA<String> badLanguage = rts.getProperty(property);
        if(badLanguage == null) throw new UnknownProperty(property);
        OneShot.CompositionalOneShot searchGraph = new OneShot.CompositionalOneShot(interpretations.stream().map((i) -> i.oneShot(badLanguage)).toList());
        Optional<Word<LetterPair<String, String>>> word = searchGraph.findAcceptedWord();
        time = System.currentTimeMillis() - time;
        JSONObject data = new JSONObject();
        if (word.isPresent()) {
            JSONArray source = new JSONArray();
            JSONArray target = new JSONArray();
            for(LetterPair<String, String> l: word.get()) {
                source.put(l.from());
                target.put(l.to());
            }
            JSONObject counterexample = new JSONObject();
            counterexample.put("source", source);
            counterexample.put("target", target);
            data.put("counterexample", counterexample);
        } else {
            data.put("result", "success");
        }
        data.put("time(ms)", time);
        data.put("exploredStates", searchGraph.getExploredStates());
        JSONObject system = rts.getStats();
        system.put("sizeOfBadLanguage", badLanguage.size());
        system.put("oneshot", data);
        system.put("call", programCall);
        System.out.println(system.toString(2));
        System.out.flush();
    }
    private static void adaptive(
            String whichInterpretations,
            String filename,
            String property,
            String programCall
    ) throws IOException, UnknownProperty {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of(filename)));
        if(rts.getTopology().equals(RegularTransitionSystem.Topology.UNKNOWN)) {
            System.err.println("There is no adaptive learning for unknown topologies.");
        }
        CompactNFA<String> badLanguage = rts.getProperty(property);
        if(badLanguage == null) throw new UnknownProperty(property);
        LinkedList<Learning.InterpretationAbstractorPair<?>> interpretations = new LinkedList<>();
        for(char i: whichInterpretations.toCharArray()) {

            switch (i) {
                case 'T' -> {
                    Interpretation<Set<String>> interpretation = new TrapInterpretationSAT(rts);
                    Abstractor<Set<String>> abstractor = null;
                    switch(rts.getTopology()) {
                        case BOW -> abstractor = new BowAbstractor<>(rts, interpretation);
                        case RING -> abstractor = new RingAbstractor<>(rts, interpretation);
                        case CROWD -> abstractor = new CrowdAbstractor<>(rts, interpretation, rts.getK()+1);
                        case UNKNOWN -> abstractor = new NoOpAbstractor<>(rts, interpretation);
                    }
                    interpretations.add(new Learning.InterpretationAbstractorPair<>(interpretation, abstractor));
                }
                case 't' -> {
                    Interpretation<Set<String>> interpretation = new TrapInterpretation(rts);
                    Abstractor<Set<String>> abstractor = null;
                    switch(rts.getTopology()) {
                        case BOW -> abstractor = new BowAbstractor<>(rts, interpretation);
                        case RING -> abstractor = new RingAbstractor<>(rts, interpretation);
                        case CROWD -> abstractor = new CrowdAbstractor<>(rts, interpretation, rts.getK()+1);
                        case UNKNOWN -> abstractor = new NoOpAbstractor<>(rts, interpretation);
                    }
                    interpretations.add(new Learning.InterpretationAbstractorPair<>(interpretation, abstractor));
                }
                case 'S' -> {
                    Interpretation<Set<String>> interpretation = new SiphonInterpretationSAT(rts);
                    Abstractor<Set<String>> abstractor = null;
                    switch(rts.getTopology()) {
                        case BOW -> abstractor = new BowAbstractor<>(rts, interpretation);
                        case RING -> abstractor = new RingAbstractor<>(rts, interpretation);
                        case CROWD -> abstractor = new CrowdAbstractor<>(rts, interpretation, rts.getK()+1);
                        case UNKNOWN -> abstractor = new NoOpAbstractor<>(rts, interpretation);
                    }
                    interpretations.add(new Learning.InterpretationAbstractorPair<>(interpretation, abstractor));
                }
                case 's' -> {
                    Interpretation<Set<String>> interpretation = new SiphonInterpretation(rts);
                    Abstractor<Set<String>> abstractor = null;
                    switch(rts.getTopology()) {
                        case BOW -> abstractor = new BowAbstractor<>(rts, interpretation);
                        case RING -> abstractor = new RingAbstractor<>(rts, interpretation);
                        case CROWD -> abstractor = new CrowdAbstractor<>(rts, interpretation, rts.getK()+1);
                        case UNKNOWN -> abstractor = new NoOpAbstractor<>(rts, interpretation);
                    }
                    interpretations.add(new Learning.InterpretationAbstractorPair<>(interpretation, abstractor));
                }
                case 'f' -> {
                    Interpretation<Set<String>> interpretation = new FlowInterpretation(rts);
                    Abstractor<Set<String>> abstractor = null;
                    switch(rts.getTopology()) {
                        case BOW -> abstractor = new BowAbstractor<>(rts, interpretation);
                        case RING -> abstractor = new RingAbstractor<>(rts, interpretation);
                        case CROWD -> abstractor = new CrowdAbstractor<>(rts, interpretation, rts.getK()+3);
                        case UNKNOWN -> abstractor = new NoOpAbstractor<>(rts, interpretation);
                    }
                    interpretations.add(new Learning.InterpretationAbstractorPair<>(interpretation, abstractor));
                }
            }
        }
        long time = System.currentTimeMillis();
        JSONObject result;
        try {
            Learning learning = new Learning(rts, interpretations, badLanguage, false);
            learning.learn();
            result = learning.getStats();
            result.put("result", "success");
        } catch (Learning.Unlearnable e) {
            result = e.stats;
            result.put("result", "failure");
            JSONArray source = new JSONArray();
            JSONArray target = new JSONArray();
            for(LetterPair<String, String> l: e.unremovable) {
                source.put(l.from());
                target.put(l.to());
            }
            JSONObject counterexample = new JSONObject();
            counterexample.put("source", source);
            counterexample.put("target", target);
            result.put("counterexample", counterexample);
        }
        time = System.currentTimeMillis() - time;
        result.put("time(ms)", time);
        JSONObject system = rts.getStats();
        system.put("sizeOfBadLanguage", badLanguage.size());
        system.put("adaptive", result);
        system.put("call", programCall);
        System.out.println(system.toString(2));
        System.out.flush();
    }

    private static void learn(
            String whichInterpretations,
            String filename,
            String property,
            String programCall,
            boolean finalCheck
    ) throws IOException, UnknownProperty {
        RegularTransitionSystem rts = RegularTransitionSystem.parseJSON(Files.readString(Path.of(filename)));
        CompactNFA<String> badLanguage = rts.getProperty(property);
        if(badLanguage == null) throw new UnknownProperty(property);
        LinkedList<Interpretation<?>> interpretations = new LinkedList<>();
        for(char i: whichInterpretations.toCharArray()) {
            switch (i) {
                case 'T' -> interpretations.add(new TrapInterpretationSAT(rts));
                case 't' -> interpretations.add(new TrapInterpretation(rts));
                case 'S' -> interpretations.add(new SiphonInterpretationSAT(rts));
                case 's' -> interpretations.add(new SiphonInterpretation(rts));
                case 'f' -> interpretations.add(new FlowInterpretation(rts));
            }
        }
        long time = System.currentTimeMillis();
        JSONObject result;
        try {
            List<Learning.InterpretationAbstractorPair<?>> l = new LinkedList<>();
            for(Interpretation<?> interpretation: interpretations) {
                l.add(Learning.InterpretationAbstractorPair.createNoOp(rts, interpretation));
            }
            Learning learning = new Learning(rts, l, badLanguage, finalCheck);
            learning.learn();
            result = learning.getStats();
            result.put("result", "success");
        } catch (Learning.Unlearnable e) {
            result = e.stats;
            result.put("result", "failure");
            JSONArray source = new JSONArray();
            JSONArray target = new JSONArray();
            for(LetterPair<String, String> l: e.unremovable) {
                source.put(l.from());
                target.put(l.to());
            }
            JSONObject counterexample = new JSONObject();
            counterexample.put("source", source);
            counterexample.put("target", target);
            result.put("counterexample", counterexample);
        }
        time = System.currentTimeMillis() - time;
        result.put("time(ms)", time);
        JSONObject system = rts.getStats();
        system.put("sizeOfBadLanguage", badLanguage.size());
        system.put("learning", result);
        system.put("call", programCall);
        System.out.println(system.toString(2));
        System.out.flush();
    }

    /**
     * The main entry point of this command-line application.
     * @param args The arguments of this command-line application.
     */
    public static void main(String[] args) {
        ArgumentParser parser = ArgumentParsers.newFor("dodo")
                .addHelp(true)
                .build();
        parser.addArgument("action")
                .action(Arguments.store())
                .choices("oneshot", "learn", "adaptive")
                .required(true);
        parser.addArgument("--noCacheOneShot")
                .action(Arguments.storeTrue());
        parser.addArgument("--finalCheck")
                .action(Arguments.storeTrue());

        Set<String> interpretationOptions = new HashSet<>();
        Set<String> trapOptions = Set.of("t", "T", "x");
        Set<String> siphonOptions = Set.of("s", "S", "x");
        Set<String> flowOptions = Set.of("f", "x");
        List<Set<String>> optionList = List.of(trapOptions, siphonOptions, flowOptions);
        Set<List<Set<String>>> allOrders = new HashSet<>();
        for(Set<String> first: optionList) {
            for(Set<String> second: optionList) {
                if(first.equals(second)) continue;
                for(Set<String> third: optionList) {
                    if(first.equals(third)) continue;
                    if(second.equals(third)) continue;
                    allOrders.add(List.of(first, second, third));
                }
            }
        }
        for(List<Set<String>> order: allOrders) {
            for(List<String> result: Sets.cartesianProduct(order)) {
                interpretationOptions.add(result.stream().reduce((x, y) -> ("x".equals(x) ? "" : x) + ("x".equals(y) ? "": y)).get());
            }
        }
        interpretationOptions.remove("");
        List<String> sortedOptions = new LinkedList<>(interpretationOptions);
        Collections.sort(sortedOptions);
        parser.addArgument("interpretations")
                .action(Arguments.store())
                .choices(sortedOptions)
                .required(true);
        parser.addArgument("file")
                .action(Arguments.store())
                .type(Arguments.fileType().verifyCanRead())
                .required(true);
        parser.addArgument("property")
                .action(Arguments.store())
                .required(true);
        try {
            Namespace arguments = parser.parseArgs(args);
            switch (arguments.getString("action")) {
                case "oneshot" -> oneshot(arguments.getString("interpretations"), arguments.getString("file"), arguments.getString("property"), Arrays.stream(args).reduce((x, y) -> x + " " + y).get(), arguments.getBoolean("noCacheOneShot"));
                case "learn" -> learn(arguments.getString("interpretations"), arguments.getString("file"), arguments.getString("property"), Arrays.stream(args).reduce((x, y) -> x + " " + y).get(), arguments.getBoolean("finalCheck"));
                case "adaptive" -> adaptive(arguments.getString("interpretations"), arguments.getString("file"), arguments.getString("property"), Arrays.stream(args).reduce((x, y) -> x + " " + y).get());
            }
        } catch (ArgumentParserException e) {
            parser.handleError(e);
        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage());
        } catch (UnknownProperty e) {
            System.err.println("The property '" + e.key + "' is unknown in this system.");
        }
    }
}
