package de.tum.in.model.dodo.algorithms;

import de.tum.in.model.dodo.specification.LetterPair;
import de.tum.in.model.dodo.specification.RegularTransitionSystem;
import net.automatalib.automata.fsa.impl.compact.CompactNFA;
import net.automatalib.words.Word;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import java.util.*;
import java.util.stream.Collectors;

public class TrapInterpretationSAT extends TrapInterpretation {
    public TrapInterpretationSAT(RegularTransitionSystem rts) {
        super(rts);
    }

    @Override
    public Word<Set<String>> disprove(RegularTransitionSystem rts, Word<String> source, Word<String> target) {
        assert source.length() == target.length();
        if(source.equals(target)) return null;
        class Indices {
            private final Map<String, Integer> alphabet;
            private final int alphabetSize;
            private final int n;
            private final ISolver solver;
            private final CompactNFA<LetterPair<String, String>> transducer;

            public Indices() {
                this.alphabet = new HashMap<>();
                this.alphabetSize = allLetters.size();
                for(String sigma: allLetters) {
                    this.alphabet.put(sigma, this.alphabet.keySet().size());
                }
                this.n = source.length();
                this.solver = SolverFactory.newDefault();
                this.transducer = rts.getTransducer();
            }

            public int getLetter(String letter, int position) {
                assert this.alphabet.containsKey(letter);
                assert 0 < position && position <= this.n;
                return (position-1)*this.alphabetSize + this.alphabet.get(letter) + 1;
            }

            public int getState(Integer state, int position, boolean presetIntersected) {
                assert 0 <= state && state < this.transducer.size();
                assert 0 <= position && position <= this.n;
                return (this.n*this.alphabetSize) + (position*this.transducer.size()*2) + 2*state + (presetIntersected ? 0 : 1) + 1;
            }

            public void addInitialClauses() {
                // mark initial states of transducer without intersection yet
                for(Integer state: this.transducer.getInitialStates()) {
                    try {
                        this.solver.addClause(new VecInt(new int[]{this.getState(state, 0, false)}));
                    } catch(ContradictionException e){
                        throw new RuntimeException("Error in propositional encoding for trap: mark initial states of transducer");
                    }
                }
            }

            public void addFinalClauses(VecInt includeSource) {
                try {
                    this.solver.addClause(includeSource);
                } catch(ContradictionException e) {
                    throw new RuntimeException("Error in propositional encoding for trap: intersection with source");
                }
                // mark no final states of the transducer with intersection
                for(Integer state: this.transducer.getStates()) {
                    if(!this.transducer.isAccepting(state)) continue;
                    try {
                        this.solver.addClause(new VecInt(new int[]{-this.getState(state, this.n, true)}));
                    } catch(ContradictionException e) {
                        throw new RuntimeException("Error in propositional encoding for trap: not mark final states of transducer");
                    }
                }
            }

            public void addClausesForStep(int step, VecInt includesSource) {
                if(0 < step) {
                    try {
                        // word excludes target
                        this.solver.addClause(new VecInt(new int[]{-this.getLetter(target.getSymbol(step - 1), step)}));
                        // word includes source
                        includesSource.push(this.getLetter(source.getSymbol(step-1), step));
                    } catch(ContradictionException e) {
                        throw new RuntimeException("Error in propositional encoding for trap: source & target in step " + step);
                    }
                }
                if(step < this.n) {
                    // inductive check
                    for(Integer state: this.transducer.getStates()) {
                        for(LetterPair<String, String> input: this.transducer.getLocalInputs(state)) {
                            for(Integer next: this.transducer.getSuccessors(state, input)) {
                                try {
                                    // no preset intersection
                                    this.solver.addClause(new VecInt(new int[] {
                                            -this.getState(state, step, false),
                                            this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, false) }));
                                    // preset intersection
                                    this.solver.addClause(new VecInt(new int[]{
                                            -this.getState(state, step, false),
                                            -this.getLetter(input.from(), step+1),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true) }));
                                    // continuous preset intersection
                                    this.solver.addClause(new VecInt(new int[]{
                                            -this.getState(state, step, true),
                                            this.getLetter(input.to(), step+1),
                                            this.getState(next, step+1, true) }));
                                } catch(ContradictionException e) {
                                    throw new RuntimeException("Error in propositional encoding for trap: check for trap in step " + step);
                                }
                            }
                        }
                    }
                }
            }

            public Word<Set<String>> extractResult() {
                try {
                    if (!this.solver.isSatisfiable()) {
                        return null;
                    }
                } catch(TimeoutException e) {
                    return null;
                }
                List<Set<String>> result = new LinkedList<>();
                for(int i = 1; i <= this.n; i++) {
                    final int position = i;
                    result.add(
                            allLetters.stream().filter((x) -> this.solver.model(this.getLetter(x, position))).collect(Collectors.toSet())
                    );
                }
                return Word.fromList(result);
            }
        }

        Indices indices = new Indices();
        indices.addInitialClauses();
        VecInt includesSource = new VecInt();
        for(int i = 0; i <= source.length(); i++) {
            indices.addClausesForStep(i, includesSource);
        }
        indices.addFinalClauses(includesSource);
        return indices.extractResult();
    }
}