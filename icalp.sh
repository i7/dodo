#!/bin/env bash

if [ $# -ne 1 ]
then
  echo "usage: <jarfile>"
  exit 1
fi

INPUTS=(
  [1]='T:benchmark/icalp-bakery.json deadlock'
  [2]='T:benchmark/icalp-bakery.json nomutex'
  [3]='T:benchmark/Burns.json deadlock'
  [4]='T:benchmark/Burns.json nomutex'
  [5]='T:benchmark/DijkstraMutEx.json deadlock'
  [6]='T:benchmark/DijkstraMutEx.json nomutex'
  [7]='T:benchmark/Dijkstra-ring.json deadlock'
  [8]='f:benchmark/Dijkstra-ring.json nomutex'
  [9]='T:benchmark/icalp-dining-cryptographers.json internal'
  [10]='T:benchmark/icalp-dining-cryptographers.json external'
  [11]='T:benchmark/icalp-Herman-line.json deadlock'
  [12]='T:benchmark/icalp-Herman-line.json notoken'
  [13]='T:benchmark/icalp-Herman-ring.json deadlock'
  [14]='T:benchmark/icalp-Herman-ring.json notoken'
  [15]='T:benchmark/icalp-Israeli-Jafon.json deadlock'
  [16]='T:benchmark/icalp-Israeli-Jafon.json notoken'
  [17]='T:benchmark/token-passing.json manytoken'
  [18]='T:benchmark/lehmann-rabin.json deadlock'
  [19]='T:benchmark/left-dining-philosophers.json deadlock'
  [20]='T:benchmark/left-dining-philosophers-remembering-sides.json deadlock'
  [21]='T:benchmark/atomic-dining-philosophers.json deadlock'
  [22]='T:benchmark/mux-array.json deadlock'
  [23]='T:benchmark/mux-array.json nomutex'
  [24]='T:benchmark/res-allocator.json deadlock'
  [25]='T:benchmark/res-allocator.json nomutex'
  [26]='T:benchmark/Berkeley.json deadlock'
  [27]='T:benchmark/Berkeley.json exclusiveunowned'
  [28]='T:benchmark/Berkeley.json exclusivenonexclusive'
  [29]='T:benchmark/Berkeley.json exclusiveexclusive'
  [30]='T:benchmark/dragon.json deadlock'
  [31]='T:benchmark/dragon.json exclusiveexclusive'
  [32]='T:benchmark/dragon.json dirtysharedexclusive'
  [33]='T:benchmark/dragon.json exclusiveshared'
  [34]='T:benchmark/dragon.json exclusivedirty'
  [35]='T:benchmark/dragon.json shareddirty'
  [36]='T:benchmark/dragon.json dirtyshareddirty'
  [37]='T:benchmark/dragon.json dirtydirty'
  [38]='T:benchmark/firefly.json deadlock'
  [39]='T:benchmark/firefly.json exclusiveexclusive'
  [40]='T:benchmark/firefly.json dirtyshared'
  [41]='T:benchmark/firefly.json dirtyexclusive'
  [42]='T:benchmark/firefly.json dirtydirty'
  [43]='T:benchmark/Illinois.json deadlock'
  [44]='T:benchmark/Illinois.json dirtyshared'
  [45]='T:benchmark/Illinois.json dirtydirty'
  [46]='T:benchmark/MESI.json deadlock'
  [47]='T:benchmark/MESI.json sharedmodified'
  [48]='T:benchmark/MESI.json modifiedmodified'
  [49]='T:benchmark/MOESI.json deadlock'
  [50]='T:benchmark/MOESI.json exclusiveexclusive'
  [51]='T:benchmark/MOESI.json sharedexclusive'
  [52]='T:benchmark/MOESI.json ownedexclusive'
  [53]='T:benchmark/MOESI.json exclusivemodified'
  [54]='T:benchmark/MOESI.json ownedmodified'
  [55]='T:benchmark/MOESI.json sharedmodified'
  [56]='T:benchmark/MOESI.json modifiedmodified'
  [57]='T:benchmark/synapse.json dirtydirty'
  [58]='T:benchmark/synapse.json dirtyvalid'
  [59]='T:benchmark/synapse.json deadlock'
)

for INDEX in ${!INPUTS[@]}
do
  echo "${INDEX} of 61"
  MODES=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$1}" - )
  FILE=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$2}" - )
  for MODE in ${MODES}
    do
      echo timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
      timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
    done
done

