#!/bin/env bash

if [ $# -ne 1 ]
then
  echo "usage: <jarfile>"
  exit 1
fi

INPUTS=(
  [0]='t s f tf sf ts tsf:benchmark/Berkeley.json exclusiveexclusive'
  [1]='t s f sf:benchmark/Berkeley.json exclusiveunowned'
  [2]='t s f sf:benchmark/Berkeley.json exclusivenonexclusive'
  [3]='t s f:benchmark/Berkeley.json deadlock'
  [4]='t s f sf:benchmark/Burns.json nomutex'
  [5]='t s f:benchmark/Burns.json deadlock'
  [6]='t s f:benchmark/Dijkstra-Scholten.json twob'
  [7]='t s f:benchmark/Dijkstra-Scholten.json twod'
  [8]='t s f:benchmark/Dijkstra-Scholten.json deadlock'
  [9]='t s f ts:benchmark/Dijkstra-ring.json nomutex'
  [10]='t s f:benchmark/Dijkstra-ring.json deadlock'
  [11]='t s f sf:benchmark/DijkstraMutEx.json nomutex'
  [12]='t s f:benchmark/DijkstraMutEx.json deadlock'
  [13]='t s f sf:benchmark/FutureBus.json pendingrightsecond'
  [14]='t s f tf sf ts tsf:benchmark/FutureBus.json sharedexclusive'
  [15]='t s f sf:benchmark/FutureBus.json secondpending'
  [16]='t s f tf sf ts tsf:benchmark/FutureBus.json exclusiveexclusive'
  [17]='t s f:benchmark/FutureBus.json deadlock'
  [18]='t s f sf:benchmark/Herman.json notoken'
  [19]='t s f tf sf ts tsf:benchmark/Illinois.json dirtydirty'
  [20]='t s f tf sf ts tsf:benchmark/Illinois.json dirtyshared'
  [21]='t s f:benchmark/Illinois.json deadlock'
  [22]='t s f sf:benchmark/Israeli-Jafon.json notoken'
  [23]='t s f sf:benchmark/MESI.json modifiedmodified'
  [24]='t s f sf:benchmark/MESI.json sharedmodified'
  [25]='t s f:benchmark/MESI.json deadlock'
  [26]='t s f sf:benchmark/MOESI.json modifiedmodified'
  [27]='t s f sf:benchmark/MOESI.json exclusiveexclusive'
  [28]='t s f sf:benchmark/MOESI.json sharedexclusive'
  [29]='t s f sf:benchmark/MOESI.json ownedexclusive'
  [30]='t s f sf:benchmark/MOESI.json exclusivemodified'
  [31]='t s f sf:benchmark/MOESI.json ownedmodified'
  [32]='t s f sf:benchmark/MOESI.json sharedmodified'
  [33]='t s f:benchmark/MOESI.json deadlock'
  [34]='t s f tf sf ts tsf:benchmark/Szymanski.json nomutex'
  [35]='t s f:benchmark/Szymanski.json deadlock'
  [35]='ts:benchmark/Szymanski.json deadlock'
  [36]='t s f sf:benchmark/atomic-dining-philosophers.json deadlock'
  [37]='t s f sf:benchmark/bakery.json nomutex'
  [38]='t s f:benchmark/dining-cryptographers.json internal'
  [39]='t s f:benchmark/dining-cryptographers.json external'
  [40]='t s f sf:benchmark/dragon.json dirtydirty'
  [41]='t s f sf:benchmark/dragon.json exclusiveexclusive'
  [42]='t s f sf:benchmark/dragon.json dirtysharedexclusive'
  [43]='t s f sf:benchmark/dragon.json exclusiveshared'
  [44]='t s f sf:benchmark/dragon.json exclusivedirty'
  [45]='t s f sf:benchmark/dragon.json shareddirty'
  [46]='t s f tf sf ts tsf:benchmark/dragon.json dirtyshareddirty'
  [47]='t s f:benchmark/dragon.json deadlock'
  [48]='t s f tf sf ts tsf:benchmark/firefly.json dirtydirty'
  [49]='t s f tf sf ts tsf:benchmark/firefly.json exclusiveexclusive'
  [50]='t s f tf sf ts tsf:benchmark/firefly.json dirtyshared'
  [51]='t s f tf sf ts tsf:benchmark/firefly.json dirtyexclusive'
  [52]='t s f:benchmark/firefly.json deadlock'
  [53]='t s f tf sf ts tsf:benchmark/left-dining-philosophers.json deadlock'
  [54]='t s f sf:benchmark/lehmann-rabin.json deadlock'
  [55]='t s f sf:benchmark/synapse.json dirtydirty'
  [56]='t s f sf:benchmark/synapse.json dirtyvalid'
  [57]='t s f:benchmark/synapse.json deadlock'
  [58]='t s f:benchmark/token-passing-no-invariant.json notoken'
  [59]='t s f ts:benchmark/token-passing-no-invariant.json manytoken'
  [60]='t s f:benchmark/token-passing.json notoken'
  [61]='t s f:benchmark/token-passing.json manytoken'
)

for INDEX in ${!INPUTS[@]}
do
  echo "${INDEX} of 61"
  MODES=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$1}" - )
  FILE=$(echo ${INPUTS[${INDEX}]} | awk -F: "{print \$2}" - )
  for MODE in ${MODES}
    do
      echo timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
      timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn ${MODE} ${FILE}
      if [[ ${MODE} =~ .*t.* || ${MODE} =~ .*s.* ]]
      then
        echo timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn $(echo ${MODE} | sed -e "s/s/S/" -e "s/t/T/") ${FILE}
        timeout 20m ~/.jdks/corretto-19.0.2/bin/java -Xmx10G -jar $1 learn $(echo ${MODE} | sed -e "s/s/S/" -e "s/t/T/") ${FILE}
      fi
    done
done

